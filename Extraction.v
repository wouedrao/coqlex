(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)


(** * Coqlex Extraction to OCaml directives *)

Require Coq.extraction.Extraction.
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.
Add LoadPath "regexp_opt" as RegExp.
Require Import ExtrOcamlNatInt RValues.
Require Import RegExp.Char.
Require Import Parser LexerDefinition CoqLexLexer CoqLexUtils.
Extraction Language OCaml.
Extraction Blacklist Char String List.
Separate Extraction Parser.prog CoqLexLexer.vlllexer lexbuf_from_string (*new_line identifier string_regex char_regex string_to_re charList_to_re regex_any ocamlStrInterp removeFirstAndLast removeFirst to_Char OPos 
new_line ret ret_f ignore raise store fcat_store fflush_store let_in sequence empty EOF default_fuel*).
