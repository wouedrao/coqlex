Artifact submission
--------------------

2 types:
	+ using git 
		URL: https://gitlab.inria.fr/wouedrao/coqlex
		branch: master
		Hash: 7ac8ca761bca139749d1a39f14aa0e636bef2703

	+ Archive VM
		SHA-256 80d03da3f90d6f7514449f728c79e481588700b575183fb41930beaab5135656
		MD5 8CA5D1CB3BB379929D05B6E385BEABAF
		Link: https://zenodo.org/record/7883260
		DOI: 10.5281/zenodo.7883260
