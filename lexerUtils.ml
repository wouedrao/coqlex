(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2021 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

open Format
open Lexing
open Stream
open LexerDefinition
open Char0

exception UserException of string 
exception SubLexemeException of string 
exception NoRegexMatch 
exception NoFuel 
exception NoTokenSpecified 

let string_of_char_list cl = String.of_seq (List.to_seq cl)

let char_list_of_string s = List.init (String.length s) (String.get s)

let to_ml_lexbuf coq_lexbuf =
    let cat = coq_lexbuf.lexeme @ coq_lexbuf.remaining_str in
 { refill_buff = (fun lexbuf -> lexbuf.lex_eof_reached <- true);
    lex_buffer =  Bytes.of_string (string_of_char_list cat);
    lex_buffer_len = List.length cat;
    lex_abs_pos = coq_lexbuf.cur_pos.abs_pos;
    lex_start_pos = 0;
    lex_curr_pos = List.length coq_lexbuf.lexeme;
    lex_last_pos = 0;
    lex_last_action = 0;
    lex_mem = [||];
    lex_eof_reached = true;
    lex_start_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos};
    lex_curr_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos};
  }
  
let set_ml_lexbuf coq_lexbuf lexbuf =
    let cat = coq_lexbuf.lexeme @ coq_lexbuf.remaining_str in
    lexbuf.lex_buffer <- Bytes.of_string (string_of_char_list cat);
    lexbuf.lex_buffer_len <- Bytes.length lexbuf.lex_buffer;
    lexbuf.lex_abs_pos <- coq_lexbuf.cur_pos.abs_pos;
    lexbuf.lex_start_pos <- 0;
    lexbuf.lex_curr_pos <- List.length coq_lexbuf.lexeme;
	lexbuf.lex_start_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos} with pos_fname = 
lexbuf.lex_start_p.pos_fname};
	lexbuf.lex_curr_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos} with pos_fname = lexbuf.lex_curr_p.pos_fname}

let from_ml_lexbuf ml_lexbuf =
 {
    s_pos = {l_number = ml_lexbuf.lex_start_p.pos_lnum; c_number = ml_lexbuf.lex_start_p.pos_bol; abs_pos = ml_lexbuf.lex_start_p.pos_cnum};
    lexeme = char_list_of_string (lexeme ml_lexbuf);
    cur_pos = {l_number = ml_lexbuf.lex_curr_p.pos_lnum; c_number = ml_lexbuf.lex_curr_p.pos_bol; abs_pos = ml_lexbuf.lex_curr_p.pos_cnum};
    remaining_str = char_list_of_string (Bytes.to_string (Bytes.sub ml_lexbuf.lex_buffer ml_lexbuf.lex_curr_pos ml_lexbuf.lex_buffer_len));
 }

let coqlexbuf_Adapter fuel coq_lexer lexbuf = 
	match coq_lexer fuel lexbuf with 
	| (AnalysisFailedUserRaisedError (err_mess, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number;
	    Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf)) -> (tok, lexbuf)
	| (AnalysisTerminated (None, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoFuel
	| (SubLexemeError (err_mess, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number;
	    Stdlib.raise (SubLexemeException (string_of_char_list err_mess))
	

let lexbuf_Adapter fuel coq_lexer ml_lexbuf = 
    let coq_lexbuf = from_ml_lexbuf ml_lexbuf in
    match coq_lexer fuel coq_lexbuf with 
	| (AnalysisFailedUserRaisedError(err_mess, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf)) -> set_ml_lexbuf lexbuf ml_lexbuf; tok
	| (AnalysisTerminated (None, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoFuel
	| (SubLexemeError(err_mess, lexbuf)) -> Format.printf "Error at position %d:%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise (SubLexemeException (string_of_char_list err_mess))

let string_to_re str = Char0.string_to_re (char_list_of_string str)

let default_starting_pos = {l_number = 1; c_number = 0; abs_pos = 0};;

let in_channel_to_string in_channel = 
	let rec in_channel_to_string_rec in_channel acc = 
		(
		  match Stdlib.input_line in_channel with
		    | exception End_of_file -> List.rev acc
		    | str -> in_channel_to_string_rec in_channel (str::acc)
		) in
	String.concat "\n" (in_channel_to_string_rec in_channel [])

let filepath_to_string path = let in_channel = Stdlib.open_in path in in_channel_to_string in_channel
                    
                    
                    
                    

