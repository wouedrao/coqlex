(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

%{
Require Import List.
Require Import Coq.Strings.Ascii Coq.Strings.String.
Import ListNotations.

Inductive RCharSet :=
| RCS : ascii -> ascii -> RCharSet.

Inductive RCharClass :=
| RCC : (list ascii) -> (list RCharSet) -> RCharClass.

Definition RCC_from_ascii_list ascii_list := RCC ascii_list [].

Definition RCC_from_charset mcharrange := RCC [] [mcharrange].

(*Definition RCC_add_ascii_list mcc ascii_list := 
match mcc with
| RCC al mr => RCC (List.app al ascii_list) mr
end.*)

(* Definition RCC_add_charset mcc mcharrange := 
match mcc with
| RCC al mr => RCC al (List.app mr mcharrange)
end. *)

Definition RCC_add mcc mcc2 :=
match mcc, mcc2 with
| RCC al mr, RCC al2 mr2 => RCC (List.app al al2) (List.app mr mr2)
end.

Inductive MRegExp := 
| MChar : ascii -> MRegExp
| MAcceptClass : RCharClass -> MRegExp
| MNotAcceptClass : RCharClass -> MRegExp
| MIdent : string -> MRegExp
| MCat : MRegExp -> MRegExp -> MRegExp
| MStar : MRegExp -> MRegExp
| MOr : MRegExp -> MRegExp -> MRegExp
| MAnd : MRegExp -> MRegExp -> MRegExp
| MMinus : MRegExp -> MRegExp -> MRegExp
| MFromStr : string -> MRegExp
| MEps : MRegExp.

Fixpoint unzip' idx (pack: list ((option string) * MRegExp)) : list MRegExp * list (string * nat) := 
  match pack with
  | [] => ([], [])
  | h::t => 
    (
      let (remRex, bindList) := unzip' (S idx) t in
      match h with
      | (None, r) => ((r::remRex), bindList)
      | (Some ids, r) => ((r::remRex), (ids, idx)::bindList)
      end
    )
  end.

Definition unzip :=  unzip' 0.

(* lexer name param1 param2 ... = elector regex {}... $(fun) {}... *)
Definition lexerData : Set := string * (list string) * string * ((list ((list MRegExp * list (string * nat)) * string)) * (list (string * string))) * bool.

(* @Group storage_type token_type lexers *)
Definition lexerGroupe : Set :=  list lexerData.

(* {header} defs lexerGroupe {trailer} *)
Definition vllStruct : Set := string * (list (string * MRegExp)) * list lexerGroupe * string.

%}

%token LPARENT RPARENT LBRACKET RBRACKET CIRCUMFLEX STAR UNDERSCORE PLUS VBAR QMARK EOF MINUS RULE AND EQ PARSE DOLLARD LET THEN SHORTEST EOF_SYMB TOKEN_POLY AS
%token <ascii> CH_LIT
%token <string> PARTCONTENT BRACECONTENT PARENTCONTENT IDENT STR_LIT

%start<vllStruct> prog
%type <list lexerGroupe> lexer_groups
%type <string> elector detect_fun param
%type <list string> params 
%type <MRegExp> regex term factor expression atom regex_list
%type <string> header bracecontent bracecontent_opt partcontent trailler
%type <(list ((list MRegExp * list (string * nat)) * string)) * (list (string * string))> lexer_definition
%type <lexerData> lexer
%type <lexerGroupe> lexer_list lexers
%type <RCharClass> characterrange characterclass 
%type <string * MRegExp> regex_bind_one
%type <list (string * MRegExp)> regex_bind_list regex_bind
%type <(option string) * MRegExp> regex_bind_one_in_rule
%type <list ((option string) * MRegExp)> regex_bind_list_in_rule
%type <bool> token_poly
%%

prog : | h = header b = regex_bind lg = lexer_groups t = trailler EOF { (h, b, lg, t) }

regex_bind :  |  { [] }
              | l = regex_bind_list { l }

regex_bind_list : | b = regex_bind_one { [b] }
                  | l = regex_bind_list b = regex_bind_one { List.app l [b] }

regex_bind_one : LET i = IDENT EQ r = regex_list { (i, r) }

bracecontent : | c = BRACECONTENT { c }

bracecontent_opt : | { ""%string }
                   | c = bracecontent  { c }
                   
partcontent : | c = PARTCONTENT { c }


header : | c = bracecontent_opt { c } | c = partcontent { c }

trailler : | c = bracecontent_opt { c } | c = partcontent { c }

lexer_groups : | g = lexers  { [g] }
               | l = lexer_groups THEN g = lexers { List.app l [g] }

lexers : | RULE l = lexer_list { l }

lexer_list :  | l = lexer { [l] }
              | ls = lexer_list AND l = lexer { List.app ls [l] }

param:
    | b = PARENTCONTENT {String.append "("%string (String.append b ")"%string)}
    | b = IDENT {b}

params :
    { [] }
    | p = param ps = params {p::ps}

token_poly: {false} | TOKEN_POLY { true }

lexer : id = IDENT p = params EQ e = elector l = lexer_definition t = token_poly { (id, p, e, l, t) }

elector :   PARSE { "LexerDefinition.longest_match_elector"%string }
          | SHORTEST { "LexerDefinition.shortest_match_elector"%string } 

lexer_definition : r = regex_bind_list_in_rule c = bracecontent { ([(unzip r, c)], []) }
                   | r = detect_fun c = bracecontent { ([], [(r, c)]) }
                   | VBAR r = regex_bind_list_in_rule c = bracecontent { ([(unzip r, c)], []) }
                   | VBAR r = detect_fun c = bracecontent { ([], [(r, c)]) }
                   | l = lexer_definition VBAR r = regex_bind_list_in_rule c = bracecontent { (List.app (fst l) [(unzip r, c)], (snd l)) }
                   | l = lexer_definition VBAR r = detect_fun c = bracecontent { ((fst l), List.app (snd l) [(r, c)]) }

detect_fun: DOLLARD c = bracecontent { c }
            | EOF_SYMB { "CoqLexUtils.EOF"%string }

regex_bind_list_in_rule : | b = regex_bind_one_in_rule { [b] }
                  | l = regex_bind_list_in_rule b = regex_bind_one_in_rule { List.app l [b] }

regex_bind_one_in_rule : r = regex AS i = IDENT { (Some i, r) }
    | r = regex { (None, r) }

regex_list : r = regex {r}
| r = regex l = regex_list {MCat r l}

regex :       | e = expression { e }

expression :  | t = term { t }
              | e = expression VBAR t = term {MOr e t}


term :        | f = factor { f }
(*              | f = factor t = term {MCat f t} *)
              | f = factor MINUS t = term {MMinus f t}

factor :      | a = atom  { a }
              | t = atom STAR {MStar t} 
              | t = atom PLUS {MCat t (MStar t)} 
              | t = atom QMARK {MOr MEps t} 

atom : | c = CH_LIT {MChar c}
       | UNDERSCORE { MIdent "RValues.regex_any"%string }
       | LPARENT e = regex_list RPARENT { e }
       | LBRACKET c = characterclass RBRACKET { MAcceptClass c }
       | LBRACKET CIRCUMFLEX c = characterclass RBRACKET { MNotAcceptClass c }
       | s = STR_LIT {MFromStr s}
       | id = IDENT { MIdent id }


characterclass :  
       | c = characterrange { c }
       | c1 = characterclass c = characterrange {RCC_add c1 c}

characterrange : 
       | c = CH_LIT { RCC_from_ascii_list [c] }
       | b = CH_LIT MINUS e = CH_LIT {RCC_from_charset (RCS b e)}
