(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex usual regexp values *)

Add LoadPath "regexp_opt" as RegExp. 

Require Import RegExp.Definitions.
Require Import RegExp.Boolean.
Require Import RegExp.Char.
Require Import Coq.Lists.List.
Require Import RegexpSimpl.
Import ListNotations.
(* Require Import Omega. *)
(* Require Import UsefullProofs. *)

Fixpoint charList_to_re l := match l with
| [] => Empty
| h::t => simp_or (Char h) (charList_to_re t)
end.


Lemma charList_to_re_correct : forall l c s, matches (charList_to_re l) (String c s) = true <-> (s = EmptyString /\ In c l).
Proof.
induction l.
simpl.
split.
intros.
assert (Empty ~!= s).
apply Empty_false.
unfold matches in *.
rewrite H0 in H.
discriminate.
intros.
destruct H.
auto.
unfold charList_to_re.
intros c s.
rewrite simp_or_correct.
split.
intros.
simpl.
simpl (charList_to_re (a :: l)) in H.
rewrite matches_Or in H.
apply Bool.orb_true_iff in H.
simpl (Char a ~== String c s) in H.
destruct (ascii_dec a c).
destruct H.
assert (s = ""%string).
case_eq s; auto.
intros.
assert (Eps ~!= (String a0 s0)).
apply Eps_false.
intuition.
inversion H1.
rewrite H0 in H.
unfold matches in H, H1.
rewrite H in H1.
discriminate.
auto.
apply IHl in H.
destruct H.
auto.
destruct H.
assert (Empty ~!= s).
apply Empty_false.
unfold matches in H, H0.
rewrite H in H0; try discriminate.
apply IHl in H.
destruct H.
split.
auto.
right.
auto.
intros.
destruct H.
inversion H0.
rewrite H.
simpl.
induction (ascii_dec a c).
auto.
auto.
simpl (charList_to_re (a :: l)).
rewrite matches_Or.
apply Bool.orb_true_iff.
right.
apply IHl.
auto.
Qed.

Fixpoint charExceptList_to_re l := match l with
| [] => AnyChar
| h::t => And (CharExcept h) (charExceptList_to_re t)
end.


Lemma charExceptList_to_re_correct : forall l c s, matches (charExceptList_to_re l) (String c s) = true <-> (s = EmptyString /\ ~ In c l).
Proof.
induction l.
+ simpl.
  split; intros.
  split; eauto.
  induction s; eauto; inversion H.
  rewrite Empty_false in H1; inversion H1.
  destruct H.
  rewrite H; simpl; auto.
+ simpl.
  split; intros.
  case_eq (ascii_dec a c); intros; auto; rewrite H0 in H; auto.
    rewrite matches_And in H.
    apply Bool.andb_true_iff in H.
    destruct H.
    rewrite Empty_false in H; inversion H.
  rewrite matches_And in H.
  apply Bool.andb_true_iff in H.
  destruct H.
  split.
  induction s; simpl in H; auto.
  rewrite Empty_false in H; inversion H.
  intuition.
  rewrite <- derivation in H1.
  apply IHl in H1.
  destruct H1.
  apply H2 in H3.
  inversion H3.
  
  destruct H.
  rewrite H.
  case_eq (ascii_dec a c)%bool; intros.
  intuition.
  rewrite matches_And.
    apply Bool.andb_true_iff.
    split; [simpl; auto|].
    rewrite <- derivation.
    apply IHl; split; auto.
Qed.
    
Definition regex_any :=  AnyChar.

Definition regex_az := CharRange "a"%char "z"%char.

Definition regex_AZ := CharRange "A"%char "Z"%char.

Definition regex_09 := CharRange "0"%char "9"%char.

Definition regex_alpha := Or regex_az regex_AZ.

Definition regex_alphanum := Or regex_alpha regex_09.

Definition num_of_ascii (c: ascii) : option nat :=
 match c with
   | "0"%char => Some 0
   | "1"%char => Some 1
   | "2"%char => Some 2
   | "3"%char => Some 3
   | "4"%char => Some 4
   | "5"%char => Some 5
   | "6"%char => Some 6
   | "7"%char => Some 7
   | "8"%char => Some 8
   | "9"%char => Some 9
   | _ => None
end.

Fixpoint reverseStringAcc s acc := match s with
| EmptyString => acc
| String h t => reverseStringAcc t (String h acc)
end.

Definition reverseString s := reverseStringAcc s ""%string.

Fixpoint num_of_string_rec (s : string) : option nat :=
  match s with
    | EmptyString => Some 0
    | String c rest => 
       match (num_of_ascii c), (num_of_string_rec rest) with
          | Some n, Some m => Some (n + 10 * m)
          | _ , _ => None
       end
   end.

Definition num_of_string s := num_of_string_rec (reverseString s).

Definition regex_0_255_3digits := 
  Or 
    (Cat (CharRange "0"%char "1"%char) (Cat (CharRange "0"%char "9"%char) (CharRange "0"%char "9"%char)))
    (Or 
      (Cat (Char "2"%char) (Cat (CharRange "0"%char "4"%char) (CharRange "0"%char "9"%char)))
      (Cat (Char "2"%char) (Cat (Char "5"%char) (CharRange "0"%char "5"%char)))).

Definition escaped_char_simple := Cat (Char "\"%char) (charList_to_re ["\"%char; """"%char; "'"%char; "n"%char; "t"%char; "b"%char; "r"%char ]).

Definition escaped_char_num := Cat (Char "\"%char) regex_0_255_3digits.

Definition ponctuation := Or (CharRange" "%char "/"%char) (Or (CharRange ":"%char "@"%char) (Or (CharRange "["%char "`"%char) (CharRange "{"%char "~"%char))).

(* Compute (matches ponctuation "["). *)

Fixpoint removeLast s := match s with
    | String e EmptyString => EmptyString
    | EmptyString => EmptyString
    | String e st => String e (removeLast st)
  end.

Definition removeFirst s := match s with
    | String e s => s
    | EmptyString => EmptyString
  end.

Definition removeFirstAndLast str := removeLast (removeFirst str).

Definition to_Char str := match str with
| String h EmptyString => Some h
| _ => None
end.

Definition identifier := (Cat regex_az (Star (Or regex_alphanum (Char "_"%char)))).

Definition printable_chars := CharRange " "%char "~"%char.

Definition string_regex := (Cat (Char """"%char) (Cat (Star (Or (Cat (Char "\"%char) regex_any ) (And (CharRangeExcept "\"%char "\"%char) (CharRangeExcept """"%char """"%char) ) )) (Char """"%char))).

Require Import MatchLenSimpl.

Fixpoint ocamlStrInterp str := match str with
| (String "\"%char (String "n"%char s)) => String "010"%char (ocamlStrInterp s)
| (String "\"%char (String "t"%char s)) => String "009"%char (ocamlStrInterp s)
| (String "\"%char (String "b"%char s)) => String "008"%char (ocamlStrInterp s)
| (String "\"%char (String """"%char s)) => String "034"%char (ocamlStrInterp s)
| (String "\"%char (String "'"%char s)) => String "039"%char (ocamlStrInterp s)
| (String "\"%char (String "\"%char s)) => String "092"%char (ocamlStrInterp s)
| (String "\"%char (String "r"%char s)) => String "013"%char (ocamlStrInterp s) 
| (String "\"%char r )
    => (match MatchLenSimpl.match_len regex_0_255_3digits r, r with
        | Some _, (String p (String p2 (String p3 s))) => 
          (match num_of_string (String p (String p2 (String p3 EmptyString))) with
           | Some v => String (ascii_of_nat v) (ocamlStrInterp s)
           | None => String ("\"%char) (ocamlStrInterp r)
          end)
        | _, _ => String ("\"%char) (ocamlStrInterp r)
        end)
| (String h t) => String h (ocamlStrInterp t)
| EmptyString => EmptyString
end.

Definition char_regex := (Cat (Char "'"%char) (Cat (Or printable_chars (Or escaped_char_num escaped_char_simple))  (Char "'"%char))).

Definition plus reg := simp_cat reg (simp_star reg).

Fixpoint re_eqb r r' := match r, r' with
| Empty, Empty => true
| Eps, Eps => true
| Char c, Char c' => (c =? c')%char
| (Cat r s), (Cat r' s') => ((re_eqb r r') && (re_eqb s s'))%bool
| (Or r s), (Or r' s') => ((re_eqb r r') && (re_eqb s s'))%bool
| (Star r), (Star r') => re_eqb r r'
| (And r s), (And r' s') => ((re_eqb r r') && (re_eqb s s'))%bool
| AnyChar, AnyChar => true
| CharExcept c, CharExcept c' => (c =? c')%char
| CharRange l h, CharRange l' h' => ((l =? l')%char && (h =? h')%char)%bool
| CharRangeExcept l h, CharRangeExcept l' h' => ((l =? l')%char && (h =? h')%char)%bool
| Minus r s, Minus r' s' => ((re_eqb r r') && (re_eqb s s'))%bool
| _, _ => false
end.

Lemma re_eqb_correct: forall r r', re_eqb r r' = true <-> r = r'.
Proof.
induction r, r'; simpl; try solve [split; intros; eauto]; try solve [split; intros; inversion H].
split; intros; [apply eqb_eq in H; subst; auto|inversion H; subst; apply eqb_refl].
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply IHr1 in H; apply IHr2 in H0; subst; auto
  |]; inversion H; apply IHr1 in H1; apply IHr2 in H2; inversion H; subst; rewrite H1; rewrite H2; auto.
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply IHr1 in H; apply IHr2 in H0; subst; auto
  |]; inversion H; apply IHr1 in H1; apply IHr2 in H2; inversion H; subst; rewrite H1; rewrite H2; auto.
split; intros; [apply IHr in H|apply IHr; inversion H]; subst; auto.
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply IHr1 in H; apply IHr2 in H0; subst; auto
  |]; inversion H; apply IHr1 in H1; apply IHr2 in H2; inversion H; subst; rewrite H1; rewrite H2; auto. 
split; intros; [apply eqb_eq in H; subst; auto|inversion H; subst; apply eqb_refl].
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply eqb_eq in H, H0; subst; auto| 
inversion H; subst; rewrite eqb_refl; rewrite eqb_refl; simpl; auto].
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply eqb_eq in H, H0; subst; auto| 
inversion H; subst; rewrite eqb_refl; rewrite eqb_refl; simpl; auto].
split; intros; [apply Bool.andb_true_iff in H; destruct H; apply IHr1 in H; apply IHr2 in H0; subst; auto
  |]; inversion H; apply IHr1 in H1; apply IHr2 in H2; inversion H; subst; rewrite H1; rewrite H2; auto. 
Qed.

Definition const_Cat a b := match a, b with
| Star a', Star b' => 
    if re_eqb a' b' then Star a' else simp_cat a b
| _, _ => simp_cat a b
end.

Lemma star_cat_star_star_r: forall r, (Star r) =R= (Cat (Star r) (Star r)).
Proof.
unfold re_eq.
intros.
case_eq (Cat (Star r) (Star r) ~= s); intros; [rewrite star_cat_star_star; auto|].
case_eq (Star r ~= s); intros; auto.
apply star_cat_star_star in H0.
rewrite H0 in H; inversion H.
Qed.

Theorem const_Cat_correct: forall a b, const_Cat a b =R= Cat a b.
Proof.
induction a, b; try apply simp_cat_correct; simpl.
case_eq (re_eqb a b); intros; auto; try apply re_eq_refl.
apply re_eqb_correct in H; subst.
apply star_cat_star_star_r.
Qed.

Definition const_Or a b := if re_eqb a b then a else simp_or a b.

Theorem const_Or_correct: forall a b, const_Or a b =R= Or a b.
Proof.
unfold const_Or.
intros.
case_eq (re_eqb a b); intros; auto; try apply simp_or_correct.
apply re_eqb_correct in H; subst.
apply re_eq_sym.
apply Or_idem .
Qed.

Definition const_And a b := if re_eqb a b then a else simp_and a b.

Lemma And_idem : forall r, And r r =R= r.
Proof.
unfold re_eq; intros.
rewrite matches_And.
apply Bool.andb_diag.
Qed.

Theorem const_And_correct: forall a b, const_And a b =R= And a b.
Proof.
unfold const_And.
intros.
case_eq (re_eqb a b); intros; auto; try apply simp_and_correct.
apply re_eqb_correct in H; subst.
apply re_eq_sym.
apply And_idem .
Qed.

Definition const_Minus a b := if re_eqb a b then Empty else simp_minus a b.

Theorem const_Minus_correct: forall a b, const_Minus a b =R= Minus a b.
Proof.
unfold const_Minus.
intros.
case_eq (re_eqb a b); intros; auto; try apply simp_minus_correct.
apply re_eqb_correct in H; subst.
unfold re_eq; intros.
rewrite matches_Minus.
rewrite Bool.andb_negb_r.
apply Empty_false.
Qed.

Definition const_Star a := simp_star a.

Theorem const_Star_correct: forall a, const_Star a =R= Star a.
Proof.
unfold const_Star.
apply simp_star_correct.
Qed.

Definition const_Char a := Char a.

Theorem const_Char_correct: forall a, const_Char a =R= Char a.
Proof.
unfold const_Char.
intros; apply re_eq_refl.
Qed.

Definition const_CharExcept a := CharExcept a.

Theorem const_CharExcept_correct: forall a, const_CharExcept a =R= CharExcept a.
Proof.
unfold const_CharExcept.
intros; apply re_eq_refl.
Qed.

Definition const_CharRange a b := if (b <? a)%char then Empty else (CharRange a b).

Theorem const_CharRange_correct: forall a b, const_CharRange a b =R= CharRange a b.
Proof.
unfold const_CharRange.
intros.
case_eq (b <? a)%char; intros.
apply re_eq_sym.
apply CharRange_equiv_empty; auto.
apply re_eq_refl.
Qed.

Definition const_CharRangeExcept a b := if (b <? a)%char then AnyChar else (CharRangeExcept a b).

Theorem const_CharRangeExcept_correct: forall a b, const_CharRangeExcept a b =R= CharRangeExcept a b.
Proof.
unfold const_CharRangeExcept.
intros.
case_eq (b <? a)%char; intros.
apply re_eq_sym.
apply CharRangeExcept_equiv_empty; auto.
apply re_eq_refl.
Qed.







