# Artifact files
- `~/Desktop/coqlex-built` directory contains all Coqlex files (source + compiled files).
    User can delete compiled files using `make clean` from  `~/Desktop/coqlex-built`
- `~/Desktop/coqlex-source` directory contains symbolic links to CoqLex source files. 
    User can read implementation and proofs using those files
    
    [x] `MachLen.v` contains the implementation and the proofs for score computation for the longest match rule.
    [x] `MachLenSimpl.v` contains the optimization of `MachLen.v`.
    [x] `ShortestLen.v` contains the implementation and the proofs for score computation for the shortest match rule.
    [x] `ShortestSimpl.v` contains the optimization of `Shortest.v`.
    [x] `regexLib` contains the definitions for Brzozowski derivatives based regexp.
    [x] `RegexpSimpl.v` contains the implementation and the proofs for regexp simplification.
    [x] `RValues.v` contains the implementation of usual regexps.
    [x] `LexerDefinition.v` contains the definitions, the implementations and proofs for election, action.
    [x] `CoqLexUtils.v` contains the definitions of usual action.
    [x] `Extraction.v` containst the Coq instruction for extraction.
    [x] `CoqLexLexer.v` contains the definition of Coqlex lexer used to build the lexer-generator.
    [x] `Parser.vy` contains the definition of Coqlex parser used to build the lexer-generator.
    [x] `lexerUtils.ml` and `parserUtils.ml` are helper files that help to use to manage lexbufs.
    [x] `coqlex.ml` is the entrypoint of the lexer-generator. It reads the input file and handle code generation.
    [x] `*.vl` contains Coqlex lexer definition.
    [x] `*.mll` contains Ocamllex lexer definition.
    [x] `*.l` contains flex lexer definition.

# Reproducing paper results
1.  Compiling *Coqlex* software: From `~/Desktop/coqlex-built`, run `make`

2. Compiling *looping-lexer* example: From `~/Desktop/coqlex-built`, run `make example_loop` and follow instructions printed on the terminal for testing.

   [x] Source links are located in `~/Desktop/coqlex-source/examples/looping`

2. Compiling *mini-cal* example: After compiling Coqlex, run `./generator ./examples/mini-cal.vl` from `~/Desktop/coqlex-built`. Then go to `~/Desktop/coqlex-built/examples/CoqLexerGeneratedForMini-cal` to observe the generated file. The OCaml version can be found in `~/Desktop/coqlex-built/example/ocamllex-examples/mini-cal/minical.mll` 
    

3. Compiling *JSON* benchmark: From `~/Desktop/coqlex-built`, run `make compare_json` and follow instructions printed on the terminal for testing.

    [x] Source links are located in `~/Desktop/coqlex-source/comparison/json`
    

4. Compiling *XML* benchmark: From `~/Desktop/coqlex-built`, run `make compare_json` and follow instructions printed on the terminal for testing.

    [x] Source links are located in `~/Desktop/coqlex-source/comparison/xml`



# Contact
*In case of problems or questions, join us by email*: ouedraogo.tertius@gmail.com
