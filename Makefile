# ********************************************************************#
#                                                                     #
#                 Coqlex verified lexer generator                     #
#                                                                     #
#  Copyright 2021 Siemens Mobility SAS and Institut National de       #
#  Recherche en Informatique et en Automatique.                       #
#  All rights reserved. This file is distributed under                #
#  the terms of the INRIA Non-Commercial License Agreement (see the   #
#  LICENSE file).                                                     #
#                                                                     #
# ********************************************************************#

COQ_SOURCES = RegexpSimpl.v MatchLen.v MatchLenSimpl.v ShortestLen.v ShortestLenSimpl.v RValues.v SubLexeme.v LexerDefinition.v CoqLexUtils.v Parser.vy CoqLexLexer.v


SOURCES = BinNums.mli Datatypes.mli Definitions.mli List0.mli Nat.mli Orders.mli OrdersTac.mli PeanoNat.mli Specif.mli String0.mli BinNums.ml Datatypes.ml Definitions.ml List0.ml Nat.ml Orders.ml OrdersTac.ml PeanoNat.ml Specif.ml String0.ml OrdersFacts.mli OrderedType.mli MSetInterface.mli FMapList.mli Char0.mli BinPos.mli BinNat.mli BinInt.mli Ascii.mli Alphabet.mli Grammar.mli Int.mli MSetAVL.mli OrdersAlt.mli RegexpSimpl.mli BinPos.ml Char0.ml OrderedType.ml OrdersAlt.ml OrdersFacts.ml MSetInterface.ml FMapList.ml BinNat.ml BinInt.ml Ascii.ml Alphabet.ml MatchLenSimpl.mli LexerDefinition.mli FSetAVL.mli FMapAVL.mli CoqLexUtils.mli Automaton.mli Interpreter_correct.mli RValues.mli Validator_complete.mli Validator_safe.mli Grammar.ml Int.ml MSetAVL.ml RegexpSimpl.ml MatchLenSimpl.ml LexerDefinition.ml FSetAVL.ml FMapAVL.ml CoqLexUtils.ml Automaton.ml Interpreter.mli Interpreter_complete.mli Main.mli Parser.mli Interpreter_correct.ml lexerUtils.ml RValues.ml Validator_complete.ml Validator_safe.ml Interpreter.ml Interpreter_complete.ml CoqLexLexer.mli Main.ml Parser.ml ParserUtils.ml CoqLexLexer.ml coqlex.ml

EXEC = generator

LIBS=unix.cma -cclib -lunix str.cma

all: $(EXEC)

OBJS = $(SOURCES:.ml=.cmo)
OPTOBJS = $(SOURCES2:.ml=.cmx)

COQ_SOURCES1 = $(COQ_SOURCES:.vy=.v)
COQ_OBJS = $(COQ_SOURCES1:.v=.vo)

$(EXEC): reg_lib $(COQ_OBJS) coqlex.ml
	@coqc Extraction.v > /dev/null
	ocamlc -w -3 -c $(SOURCES) > /dev/null
	ocamlc -o $(EXEC) $(LIBS) $(OBJS) > /dev/null

reg_lib: regexp_opt
	@$(MAKE) -C $^ > /dev/null

example_json: $(EXEC)
	@echo "Generating coqlex JSON lexer"
	@./$(EXEC) example/json.vl > /dev/null
	@echo "Compiling ocamllex JSON lexer"
	@cd example/ocamllex-examples/JSON; ./compile.sh > /dev/null
	@echo "Compiling coqlex JSON lexer"
	@cd example/CoqLexerGeneratedForJson; ./compile.sh > /dev/null
	@echo "From 'example/ocamllex-examples/JSON' or 'example/CoqLexerGeneratedForJson', run './analyzer -lexer read [file_path]' to execute the lexer. '.json' files in 'example/data-for-lexing' can be used with those lexers."

example_loop: $(EXEC)
	@echo "Generating coqlex Looping lexer example"
	@./$(EXEC) example/loopingLexer.vl > /dev/null
	@echo "Compiling ocamllex Looping lexer example"
	@cd example/ocamllex-examples/Looping; ./compile.sh > /dev/null
	@echo "Compiling coqlex Looping lexer example"
	@cd example/CoqLexerGeneratedForLoopingLexer; ./compile.sh > /dev/null
	@echo "From 'example/ocamllex-examples/Looping' or 'example/CoqLexerGeneratedForLoopingLexer', run './analyzer -lexer my_lexer [file_path]' to execute the lexer. 'example/data-for-lexing/looping-diverges.txt' and 'example/data-for-lexing/looping-terminates.txt' can be used with those lexers."

example_miniml: $(EXEC)
	@echo "Generating coqlex Mini-ml lexer"
	@./$(EXEC) example/miniml.vl > /dev/null
	@echo "Compiling ocamllex Mini-ml lexer"
	@cd example/ocamllex-examples/MINI-ML; ./compile.sh > /dev/null
	@echo "Compiling coqlex Mini-ml lexer"
	@cd example/CoqLexerGeneratedForMiniml; ./compile.sh > /dev/null
	@echo "From 'example/ocamllex-examples/MINI-ML' or 'example/CoqLexerGeneratedForMiniml', run './analyzer -lexer token_lexer [file_path]' to execute the lexer. '.ml' files in 'example/data-for-lexing' can be used with those lexers."
	
compare_json: $(EXEC)
	@echo "----------------------------"
	@echo "Generating CoqLex Json lexer"
	@echo "----------------------------"
	@./$(EXEC) Comparison/JSON/Lexers/CoqLex/json_lexer.vl --no-main -o Comparison/JSON/Lexers/CoqLex > /dev/null
	@echo "----------------"
	@echo "Performing tests"
	@echo "----------------"
	@cd Comparison/JSON; ./plot.sh
    
compare_xml: $(EXEC)
	@echo "----------------------------"
	@echo "Generating CoqLex XML lexer"
	@echo "----------------------------"
	@./$(EXEC) Comparison/XML/Lexers/CoqLex/xml.vl --no-main -o Comparison/XML/Lexers/CoqLex > /dev/null
	@echo "----------------"
	@echo "Performing tests"
	@echo "----------------"
	@cd Comparison/XML; ./plot.sh

.SUFFIXES: .v .vy .vo

.v.vo:
	coqc $< -R regexp_opt RegExp

.vy.v:
	menhir --coq -v $<

coq : $(COQ_OBJS)

clean:
	rm -f *.cm[iox] *~ .*~ #*#
	rm -f $(EXEC)
	rm -f $(EXEC).opt
	rm -f *.glob *.vo .*.aux *.tmp *.crashcoqide *.vos *.vok regexp_opt/*.glob regexp_opt/*.vo regexp_opt/.*.aux regexp_opt/*.tmp regexp_opt/*.crashcoqide regexp_opt/*.vos regexp_opt/*.vok Bool.mli Bool.ml  CoqLexUtils.ml CoqLexUtils.mli BinNums.mli Datatypes.mli Definitions.mli List0.mli Nat.mli Orders.mli OrdersTac.mli PeanoNat.mli Specif.mli String0.mli OrdersFacts.mli OrderedType.mli MSetInterface.mli FMapList.mli Char0.mli BinPos.mli BinNat.mli BinInt.mli Ascii.mli Alphabet.mli String0.ml Specif.ml Nat.ml List0.ml Datatypes.ml BinNums.ml BinPos.ml Definitions.ml Orders.ml OrdersTac.ml PeanoNat.ml Grammar.mli Int.mli MSetAVL.mli OrdersAlt.mli RegexpSimpl.mli MatchLenSimpl.mli LexerDefinition.mli FSetAVL.mli FMapAVL.mli Automaton.mli OrdersFacts.ml OrderedType.ml MSetInterface.ml FMapList.ml Char0.ml BinNat.ml BinInt.ml Ascii.ml Alphabet.ml Grammar.ml Int.ml MSetAVL.ml OrdersAlt.ml RegexpSimpl.ml Interpreter_correct.mli RValues.mli Validator_complete.mli Validator_safe.mli Interpreter.mli Interpreter_complete.mli MatchLenSimpl.ml LexerDefinition.ml FSetAVL.ml FMapAVL.ml Automaton.ml Interpreter_correct.ml RValues.ml Validator_complete.ml Validator_safe.ml Main.mli Parser.mli CoqLexLexer.mli Interpreter.ml Interpreter_complete.ml Main.ml Parser.ml CoqLexLexer.ml *.o Parser.automaton Parser.conflicts Parser.v

