(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

let current_year = 1900 + (Unix.gmtime (Unix.time ())).Unix.tm_year

let copy_right = Format.sprintf {|(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright %d Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)|} current_year

open ParserUtils
open LexerUtils
open Parser
open CoqLexLexer
open Definitions
open RValues
open Unix
open Sys
open Str

(** Utils **)
let buffer_size = 8192;;
let buffer = Bytes.create buffer_size;;

let file_copy input_name output_name =
  let fd_in = openfile input_name [O_RDONLY] 0 in
  let fd_out = openfile output_name [O_WRONLY; O_CREAT; O_TRUNC] 0o777 in
  let rec copy_loop () = match read fd_in buffer 0 buffer_size with
    |  0 -> ()
    | r -> ignore (write fd_out buffer 0 r); copy_loop ()
  in
  copy_loop ();
  close fd_in;
  close fd_out;;

let set_filename (fname:string) (lexbuf: Lexing.lexbuf)  =
    lexbuf.lex_curr_p <-
        { lexbuf.lex_curr_p with pos_fname = fname }
    ; lexbuf


let create_directory ?perm:(perm = 0o777) dirname =
	if (file_exists dirname) then
			if not (is_directory dirname) then
				failwith ("Please remove your file named " ^ dirname)
			else
				()
		else
			Unix.mkdir dirname perm

(** Definition of the parser module **)

module VlParser =
struct
	module M = ParserUtils.GParser(struct let default_fuel = 1000000 let coq_lexer = CoqLexLexer.vlllexer let menhirParser = Parser.prog end);;
	let parse_from_mllexbuf = M.parse_from_mllexbuf;;
	let parse_from_coqlexbuf = M.parse_from_coqlexbuf ;;
	let mlparse_from_file path = let lexbuf = set_filename path (Lexing.from_string (filepath_to_string path)) in parse_from_mllexbuf lexbuf;;
	let coqparse_from_file path = let lexbuf = LexerDefinition.lexbuf_from_string (char_list_of_string (filepath_to_string path)) in parse_from_coqlexbuf lexbuf;;
end;;

(** Code generator -- Begin **)

(*** Printing chars ***)
(* printing a Coq char *)
let char_print h = 
    let code = Char.code h in
    if code >= 32 && code <= 126 && code <> 34 then
        Format.sprintf {| "%c"%%char |} h
    else
        Format.sprintf {| "%03i"%%char |} code

(* printing a list of Coq char *)    
let rec charlist_print f cl = match cl with
| [] -> Format.fprintf f ""
| h::[] -> Format.fprintf f "%s" (char_print h)
| h::t -> Format.fprintf f "%s; %a" (char_print h) charlist_print t

(* printing a Coq string using string data type *)
let rec print_coq_string_b f clist = match clist with
| [] -> Format.fprintf f {|EmptyString|}
| h::t -> Format.fprintf f "(String %s %a)" (char_print h) print_coq_string_b t

(* printing a Coq string using notation *)
let rec print_coq_string_s f clist = match clist with
| [] -> Format.fprintf f ""
| h::t -> Format.fprintf f "%c%a" h print_coq_string_s t

(* printing a Coq string *)
let print_coq_string f clist = 
    let char_print_b h = let code = Char.code h in code >= 32 && code <= 126 && code <> 34 in
    if List.for_all char_print_b clist then Format.fprintf f "\"%a\"" print_coq_string_s clist
    else print_coq_string_b f clist

(*** Printing regexps ***)
let rec rlist_print f rlist = match rlist with
| [] -> Format.fprintf f "(*Should not happend*)"
| RCS(s, e)::[] -> Format.fprintf f "(RValues.const_CharRange %s %s)" (char_print s) (char_print e)
| RCS(s, e)::t -> Format.fprintf f "(RegexpSimpl.simp_or (RValues.const_CharRange %s %s) %a)" (char_print s) (char_print e) rlist_print t

let rec relist_print f rlist = match rlist with
| [] -> Format.fprintf f "(*Should not happend*)"
| RCS(s, e)::[] -> Format.fprintf f "(RValues.const_charRangeExcept %s %s)" (char_print s) (char_print e)
| RCS(s, e)::t -> Format.fprintf f "(RegexpSimpl.simp_and (RValues.const_CharRangeExcept %s %s) %a)" (char_print s) (char_print e) relist_print t

let mclass_print f mclass = match mclass with
| RCC ([], []) ->  Format.fprintf f "(*Should not happend*)"
| RCC ([], rlist) ->  Format.fprintf f "%a" rlist_print rlist
| RCC (alist, []) ->  Format.fprintf f "(RValues.charList_to_re [%a])" charlist_print alist
| RCC (alist, rlist) ->  Format.fprintf f "(RegexpSimpl.simp_or (RValues.charList_to_re [%a]) %a)" charlist_print alist rlist_print rlist

let meclass_print f mclass = match mclass with
| RCC ([], []) ->  Format.fprintf f "(*Should not happend*)"
| RCC ([], rlist) ->  Format.fprintf f "%a" relist_print rlist
| RCC (alist, []) ->  Format.fprintf f "(RValues.charExceptList_to_re [%a])" charlist_print alist
| RCC (alist, rlist) ->  Format.fprintf f "(RegexpSimpl.simp_and (RValues.charExceptList_to_re [%a]) %a)" charlist_print alist relist_print rlist

let rec mregex_print f regex = match regex with
| MChar c -> Format.fprintf f "Char %s" (char_print c)
| MEps -> Format.fprintf f "Eps"
| MCat (r1, r2) -> Format.fprintf f "RegexpSimpl.simp_cat (%a) (%a)" mregex_print r1 mregex_print r2
| MOr (r1, r2) -> Format.fprintf f "RegexpSimpl.simp_or (%a) (%a)" mregex_print r1 mregex_print r2
| MStar (r1) -> Format.fprintf f "RegexpSimpl.simp_star (%a)" mregex_print r1
| MAnd (r1, r2) -> Format.fprintf f "RegexpSimpl.simp_and (%a) (%a)" mregex_print r1 mregex_print r2
| MMinus (r1, r2) -> Format.fprintf f "RegexpSimpl.simp_minus (%a) (%a)" mregex_print r1 mregex_print r2
| MIdent s ->  Format.fprintf f "%s" (string_of_char_list s)
| MFromStr s ->  Format.fprintf f "RegExp.Char.string_to_re %a" print_coq_string s
| MAcceptClass c -> Format.fprintf f "%a" mclass_print c
| MNotAcceptClass c -> Format.fprintf f "%a" meclass_print c

(*** Printing regex bindings ***)
let rec process_bind_list f bind_list = match bind_list with
| [] -> Format.fprintf f ""
| (id, value)::t -> Format.fprintf f "Definition %s := %a.@.%a" (string_of_char_list id) mregex_print value process_bind_list t ;;


(*** Utils - Setting fuel parameters in actions ***)
let id_re = Str.regexp {|[_a-zA-Z]\(\.?[_a-zA-Z0-9]\)*|}

let reg_f current_defined_lexers all_lexers ident =
    if List.mem ident current_defined_lexers then
        Printf.sprintf "(%s n)" ident
    else if List.mem ident all_lexers then
        Printf.sprintf "(%s fuel)" ident
    else
        ident

let opt_search_forward re s pos =
  try Some(Str.search_forward re s pos) with Not_found -> None

let global_substitute expr repl_fun text =
  let rec replace accu start last_was_empty =
    let startpos = if last_was_empty then start + 1 else start in
    if startpos > String.length text then
      string_after text start :: accu
    else
      match opt_search_forward expr text startpos with
      | None ->
          string_after text start :: accu
      | Some pos ->
          let end_pos = match_end () in
          let found = String.sub text pos (end_pos-pos) in
          let repl_text = repl_fun found in
          replace (repl_text :: String.sub text start (pos-start) :: accu)
                  end_pos (end_pos = pos)
  in
    String.concat "" (List.rev (replace [] 0 false))
    
(*** Code generation/Utils - From lexer group list to list of lexer names + parameters ***)
let extract_all_lexers_from_lg lg =
    let rec extract_all_lexers_from_lg_ acc  = function
    | [] -> acc
    | ((((s, p), _), _), _)::t -> extract_all_lexers_from_lg_ ((string_of_char_list s, List.map string_of_char_list p)::acc) t
    in
    extract_all_lexers_from_lg_ [] lg

(*** Code generation/Utils - From .vl file to lexer group ***)
let extract_all_lexers vll = match vll with
| (((_, _), lg), _) ->  List.concat (List.map extract_all_lexers_from_lg lg)

(*** Code generation - Set SubLexeme bindings ***)

let rec access_sublexem_array f bindings = match bindings with
| [] -> ()
| (_, i)::[] -> Format.fprintf f {|(List.nth_error sublexemes %d)|} i
| (_, i)::t -> Format.fprintf f {|(List.nth_error sublexemes %d), %a|} i access_sublexem_array t

let rec joker_sublexem_array f bindings = match bindings with
| [] -> ()
| _::[] -> Format.fprintf f {|_ |}
| _::t -> Format.fprintf f {|_, %a|} joker_sublexem_array t

let rec sublexem_name_some f bindings = match bindings with
| [] -> ()
| (vid, _)::[] -> Format.fprintf f {|(Some %s)|} (string_of_char_list vid)
| (vid, _)::t -> Format.fprintf f {|(Some %s), %a|} (string_of_char_list vid) sublexem_name_some t

let rec mregexList_print f regex = match regex with
| [] -> ()
| [h] -> mregex_print f h
| h::t -> Format.fprintf f "%a; %a" mregex_print h mregexList_print t

let generateBindingsInActions f (re, bindings, action) = if bindings = [] then Format.fprintf f {|%s|} action else
Format.fprintf f {| @[<v 8>  (fun lexbuf => @ let sublexemes_opt := SubLexeme.split lexbuf.(lexeme) [%a] in@ match sublexemes_opt with@ | None => SubLexemeError "Fatal error: Could not unpack sub-lexemes" lexbuf@ | Some sublexemes =>  (@[<v>@ match %a with@ | %a => %s lexbuf@ | %a => SubLexemeError "Fatal error: Could not unpack some sub-lexemes" lexbuf@ end@ )@] @ end@]@   )@ |} mregexList_print re access_sublexem_array bindings sublexem_name_some bindings action joker_sublexem_array bindings

(*** Code generation - Printing (regexp * action) list  ***)
let rec mregListTomregex = function 
| [] -> MEps
| [h] -> h
| h::t -> MCat(h, mregListTomregex t)

let rec process_regexXaction f (current_defined_lexers, all_lexers, rXa) = match rXa with
| [] -> ()
| ((regex, rbindings), action)::[] -> Format.fprintf f "(%a, %a)" mregex_print (mregListTomregex regex) generateBindingsInActions (regex, rbindings, global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list action))
| ((regex, rbindings), action)::t -> Format.fprintf f "(%a, %a);@ %a" mregex_print (mregListTomregex regex)
    generateBindingsInActions (regex, rbindings, global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list action))
    process_regexXaction (current_defined_lexers, all_lexers, t) ;;

(*** Code generation - Printing (fun * action) list  ***)
let rec process_funXaction f (current_defined_lexers, all_lexers, rXa) = match rXa with
| [] -> ()
| (fu, action)::[] -> Format.fprintf f "(%s, %s)" (global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list fu)) (global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list action))
| (fu, action)::t -> Format.fprintf f "(%s, %s);@ %a" (Str.global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list fu)) (global_substitute id_re (reg_f (List.map fst current_defined_lexers) (List.map fst all_lexers)) (string_of_char_list action)) process_funXaction (current_defined_lexers, all_lexers, t) ;;

(*** Code generation/Utils - For polymorphic token ***)
let process_t_policy_declaration f b = if b then Format.fprintf f "{Token : Set}" else ()

let process_t_policy_generalize f b = if b then Format.fprintf f "(Action := LexerDefinition.semantic_action (Token := Token))" else ()

(*** Code generation - Printing params ***)
let rec params_print f ls = match ls with
| [] -> ()
| h::t -> Format.fprintf f "%s %a" (string_of_char_list h) params_print t

(*** Code generation - Printing a lexer ***)
let process_lexer f (is_fst_lexer, l_name, params, el_type, (regexpXaction, funXaction), tok_poly, (current_defined_lexers, all_lexers)) =
    Format.fprintf f
{code|%s %s %a fuel %a lexbuf {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf)
| S n =>
  (@[<v 8>    match LexerDefinition.generalizing_elector %a %s ([@ %a],@ [%a]@ )@] (remaining_str lexbuf) with
    | Some elt => LexerDefinition.exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end
|code}
    (if is_fst_lexer then "Fixpoint" else "with")
    (string_of_char_list l_name)
    process_t_policy_declaration tok_poly
    params_print params
    process_t_policy_generalize tok_poly
    (string_of_char_list el_type)
    process_regexXaction (current_defined_lexers, all_lexers, regexpXaction)
    process_funXaction (current_defined_lexers, all_lexers, funXaction)

(*** Code generation - Printing a group of lexers ***)
let rec process_lexer_group f (is_first, group, current_defined_lexers, all_lexers) = match group with
| [] -> Format.fprintf f "."
| (((((l_name, l_params), el_type), (regexpXaction, funXaction)), tok_poly))::t -> Format.fprintf f "%a%a" process_lexer (is_first, l_name, l_params, el_type, (regexpXaction, funXaction), tok_poly, (current_defined_lexers, all_lexers)) process_lexer_group (false, t, current_defined_lexers, all_lexers)

(*** Code generation - Printing list of groups of lexers ***)
let rec process_lexer_list_of_group f (group_list, all_lexers) = match group_list with
| [] -> ()
| h::t ->
    let current_defined_lexers = extract_all_lexers_from_lg h in
    Format.fprintf f "%a@.%a" process_lexer_group (true, h, current_defined_lexers, all_lexers)
    process_lexer_list_of_group (t, current_defined_lexers @ all_lexers)

(*** Code generation - Printing list of strings ***)
let rec printlist f lst  = match lst with
| [] -> ()
| h::t -> Format.fprintf f "%s %a" h printlist t ;;


(*** Code generation - Printing a vl file ***)
let codeGenerate f (((header, reg_def), list_of_group), trailler) filename_bn = Format.fprintf f {|%s

Add LoadPath "RegExpLib" as RegExp.
Require Import LexerDefinition RValues RegExp.Definitions RegExp.Char List CoqLexUtils SubLexeme RegexpSimpl.
Require Import String.
Import ListNotations.
Local Open Scope string_scope.

(*Header*)
%s

(*Regexp bindings*)
%a

(*Lexer definitions*)
%a

(*Trailler*)
%s

Require Coq.extraction.Extraction ExtrOcamlBasic ExtrOcamlChar ExtrOcamlString ExtrOcamlNatInt.
Extraction Blacklist Char String List.
Extraction "%s" lexbuf_from_string %a.|} copy_right
    (string_of_char_list header)
    process_bind_list reg_def
    process_lexer_list_of_group (list_of_group, [])
    (string_of_char_list trailler)
    (filename_bn ^ ".ml")
    printlist (List.map fst (List.concat (List.map extract_all_lexers_from_lg list_of_group)))


(*** Code generation/Utils - Printing dummy params ***)
let rec dummy_params_print f n = 
    if n <= 0 then () else Format.fprintf f "p_%d %a" n dummy_params_print (n-1)


(*** Code generation - Printing an OCAML version of the lexers  ***)
let rec generateLexerModule f (module_name, lexer_list) = match lexer_list with
| [] -> ()
| (h, params)::t -> 
    let len = List.length params in 
    Format.fprintf f "let %s_ocamllexer_wf = fun %al -> lexbuf_Adapter default_fuel (fun f l -> %s.%s f %al) l@."
    h dummy_params_print len module_name h dummy_params_print len;
    Format.fprintf f "let %s_coqlexer_wf = fun %al -> coqlexbuf_Adapter default_fuel (fun f l -> %s.%s f %al) l@."
    h dummy_params_print len module_name h dummy_params_print len;
    Format.fprintf f "%a" generateLexerModule (module_name, t) ;;

(*** Code generation - Printing OCAML Utils for lexers  ***)
let generateUtilCode f (module_name, vll, userFuel) = Format.fprintf f {|%s

open Format
open Lexing
open Stream
open %s

exception UserException of string 
exception SubLexemeException of string 
exception NoRegexMatch 
exception NoFuel 
exception NoTokenSpecified 

let string_of_char_list cl = String.of_seq (List.to_seq cl)

let char_list_of_string s = List.init (String.length s) (String.get s)

let to_ml_lexbuf coq_lexbuf =
    let cat = coq_lexbuf.lexeme @@ coq_lexbuf.remaining_str in
 { refill_buff = (fun lexbuf -> lexbuf.lex_eof_reached <- true);
    lex_buffer =  Bytes.of_string (string_of_char_list cat);
    lex_buffer_len = List.length cat;
    lex_abs_pos = coq_lexbuf.cur_pos.abs_pos;
    lex_start_pos = 0;
    lex_curr_pos = List.length coq_lexbuf.lexeme;
    lex_last_pos = 0;
    lex_last_action = 0;
    lex_mem = [||];
    lex_eof_reached = true;
    lex_start_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos};
    lex_curr_p = {pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos};
  }
  
let set_ml_lexbuf coq_lexbuf lexbuf =
    let cat = coq_lexbuf.lexeme @@ coq_lexbuf.remaining_str in
    lexbuf.lex_buffer <- Bytes.of_string (string_of_char_list cat);
    lexbuf.lex_buffer_len <- Bytes.length lexbuf.lex_buffer;
    lexbuf.lex_abs_pos <- coq_lexbuf.cur_pos.abs_pos;
    lexbuf.lex_start_pos <- 0;
    lexbuf.lex_curr_pos <- List.length coq_lexbuf.lexeme;
	lexbuf.lex_start_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.s_pos.l_number; pos_bol = coq_lexbuf.s_pos.c_number; pos_cnum = coq_lexbuf.s_pos.abs_pos} with pos_fname = 
lexbuf.lex_start_p.pos_fname};
	lexbuf.lex_curr_p <- {{pos_fname = ""; pos_lnum  = coq_lexbuf.cur_pos.l_number; pos_bol = coq_lexbuf.cur_pos.c_number; pos_cnum = coq_lexbuf.cur_pos.abs_pos} with pos_fname = lexbuf.lex_curr_p.pos_fname}

let from_ml_lexbuf ml_lexbuf =
 {
    s_pos = {l_number = ml_lexbuf.lex_start_p.pos_lnum; c_number = ml_lexbuf.lex_start_p.pos_bol; abs_pos = ml_lexbuf.lex_start_p.pos_cnum};
    lexeme = char_list_of_string (lexeme ml_lexbuf);
    cur_pos = {l_number = ml_lexbuf.lex_curr_p.pos_lnum; c_number = ml_lexbuf.lex_curr_p.pos_bol; abs_pos = ml_lexbuf.lex_curr_p.pos_cnum};
    remaining_str = char_list_of_string (Bytes.to_string (Bytes.sub ml_lexbuf.lex_buffer ml_lexbuf.lex_curr_pos ml_lexbuf.lex_buffer_len));
 }

let coqlexbuf_Adapter fuel coq_lexer lexbuf = 
	match coq_lexer fuel lexbuf with 
	| (AnalysisFailedUserRaisedError (err_mess, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number;
	    Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf)) -> (tok, lexbuf)
	| (AnalysisTerminated (None, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; Stdlib.raise NoFuel
	| (SubLexemeError (err_mess, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number;
	    Stdlib.raise (SubLexemeException (string_of_char_list err_mess))
	

let lexbuf_Adapter fuel coq_lexer ml_lexbuf = 
    let coq_lexbuf = from_ml_lexbuf ml_lexbuf in
    match coq_lexer fuel coq_lexbuf with 
	| (AnalysisFailedUserRaisedError(err_mess, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise (UserException (string_of_char_list err_mess))
	| (AnalysisFailedEmptyToken (lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoRegexMatch
	| (AnalysisTerminated (Some tok, lexbuf)) -> set_ml_lexbuf lexbuf ml_lexbuf; tok
	| (AnalysisTerminated (None, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoTokenSpecified
	| (AnalysisNoFuel lexbuf) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise NoFuel
	| (SubLexemeError(err_mess, lexbuf)) -> Format.printf "Error at position %%d:%%d@." lexbuf.cur_pos.l_number lexbuf.cur_pos.c_number; set_ml_lexbuf lexbuf ml_lexbuf; Stdlib.raise (SubLexemeException (string_of_char_list err_mess))

let in_channel_to_string in_channel = 
	let rec in_channel_to_string_rec in_channel acc = 
		(
		  match Stdlib.input_line in_channel with
		    | exception End_of_file -> List.rev acc
		    | str -> in_channel_to_string_rec in_channel (str::acc)
		) in
	String.concat "\n" (in_channel_to_string_rec in_channel [])

let filepath_to_string path = let in_channel = Stdlib.open_in path in in_channel_to_string in_channel

let default_fuel = %d
(*Lexers*)
%a
|} copy_right
module_name
userFuel
generateLexerModule (module_name, extract_all_lexers vll)
;;


(*** Code generation/Utils - Printing params for the lexers of the benchmark file ***)
let params_print_bench f params = if params = [] then () else Format.fprintf f {|(failwith "please, provide %d additional parameter(s) necessary for this test")|} (List.length params) 

(*** Code generation/Utils - Generating lexers tests in the benchmark file ***)
let rec generateLexerBench f lexer_list = match lexer_list with
| [] -> Format.fprintf f {|
            | _ -> failwith "Lexer not found"|}
| (h, params)::t -> Format.fprintf f {|
            | "%s" ->
                let count = ref 0 in
                let rec loop lexbuf =
                (
                    match %s_coqlexer_wf %a lexbuf with
                    | (t, lb) -> count := !count + 1; if lb.remaining_str <> [] then loop lb
                )
                in
                let _ = Format.printf "Starting evaluation of %%s on %%s@@." !lexer_name path in
                let start = Unix.gettimeofday() in
                let _ = loop lexbuf in
                let end_ = Unix.gettimeofday() in
                Format.printf "Time = %%f (for %%d tokens)@@." (end_ -. start) !count %a|}
    h
    h
    params_print_bench params
    generateLexerBench t

(*** Code generation - Extracting the name of the last lexer ***)
let rec getLastLexerName lexer_list = match lexer_list with
| [] -> ""
| (h, params)::[] -> h
| h::t -> getLastLexerName t

(*** Code generation - Generating a benchmark file ***)
let mainMLGenerateUsual f (lexer_list, module_name) = Format.fprintf f {|%s

open Format
open %s
open Lib

let () =
    let usage_msg = Format.sprintf "A program to run a benchmark on a Coq-lexer.@;Usage : %%s filename -lexer_name string@@." Sys.argv.(0) in
    let inputPath = ref None in
    let lexer_name = ref "%s" in
    let speclist =
        [
            ("-lexer", Arg.Set_string lexer_name, "The name of the lexer to benchmark")
        ] in
    Arg.parse speclist (fun anon -> inputPath := Some anon) usage_msg;
    match !inputPath with
    | None -> failwith "No input file"
    | Some path ->
        let _ = Format.printf "Reading input file@@." in
        let input_string = filepath_to_string path in
        let lexbuf = lexbuf_from_string (char_list_of_string input_string) in
        (match !lexer_name with %a)|} copy_right
        module_name
        (getLastLexerName lexer_list)
        generateLexerBench lexer_list ;;

(*** Code generation - Generating a compilation script ***)
let compileSHGenerate f module_name = Format.fprintf f {|#(* *********************************************************************)
#(*                                                                     *)
#(*                 Coqlex verified lexer generator                     *)
#(*                                                                     *)
#(*  Copyright %d Siemens Mobility SAS and Institut National de       *)
#(*  Recherche en Informatique et en Automatique.                       *)
#(*  All rights reserved. This file is distributed under                *)
#(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
#(*  LICENSE file).                                                     *)
#(*                                                                     *)
#(* *********************************************************************)
#!/bin/bash
coqc %s.v
ocamlopt -alert "-deprecated" unix.cmxa -cclib -lunix %s.mli %s.ml Lib.ml main.ml -o analyzer
rm -f %s.vos *.vok %s.ml %s.mli *.cmo *.cmi .*.aux *.o *.cmx *.glob 
|} current_year module_name  module_name module_name module_name module_name module_name

;;

(** End of code generation functions**)

(** entrypoint **)
let () =
	begin
		let usage_msg = Format.sprintf "A program to generate a Coq-lexer .@;Usage : %s filename" Sys.argv.(0) in
		let inputPath = ref None in
		let ignore_main = ref false in
		let ignore_libml = ref false in
		let userFuel = ref 1000000 in
		let out_dir = ref "" in
		let speclist = [
		    ("--no-main", Arg.Set ignore_main, "Prevent the generation of main.ml and compile.sh"); 
		    ("--no-lib-ml", Arg.Set ignore_libml, "Prevent the generation of Lib.ml"); 
		    ("--fuel", Arg.Set_int  userFuel, "Default fuel of Coqlex lexers"); 
		    ("-o", Arg.Set_string out_dir, "Set output directory name"); 
	    ] in
		Arg.parse speclist (fun anon -> inputPath := Some anon) usage_msg;
		match !inputPath with
		| None -> failwith "No input file"
		| Some path ->
		        let prog_dirname = Filename.dirname (Unix.realpath (Sys.argv.(0))) in
		        let base_name_wo_ext = Filename.remove_extension (Filename.basename path) in
		        let module_name = String.capitalize_ascii base_name_wo_ext in
				Format.printf "log------------------------@.";
		        Format.printf "Parsing %s@." path;
		        let parsed = VlParser.coqparse_from_file path in
				Format.printf "%s is parsed@." path;
				let inputfile_dirname = if !out_dir <> "" then !out_dir else (Filename.concat (Filename.dirname path) (Format.sprintf "CoqLexerGeneratedFor%s" module_name)) in
				Format.printf "Creating directory: %s@." inputfile_dirname;
				create_directory inputfile_dirname;
				Format.printf "Generating file %s.v@." module_name;
				let out_channel = open_out (Filename.concat inputfile_dirname ( module_name ^ ".v")) in
				codeGenerate (Format.formatter_of_out_channel out_channel) parsed module_name; close_out out_channel;
				if not !ignore_libml then
				(
				    Format.printf "Generating file Lib.ml@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname ( "Lib.ml")) in
				    generateUtilCode (Format.formatter_of_out_channel out_channel) (module_name, parsed, !userFuel); close_out out_channel
			    );
				if not !ignore_main then
				(
				    Format.printf "Generating file compile.sh@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname "compile.sh") in
				    compileSHGenerate (Format.formatter_of_out_channel out_channel) module_name; close_out out_channel;
				    chmod (Filename.concat inputfile_dirname "compile.sh") 0o740;
				    Format.printf "Generating file main.ml@.";
				    let out_channel = open_out (Filename.concat inputfile_dirname ( "main.ml")) in
				    mainMLGenerateUsual (Format.formatter_of_out_channel out_channel) (extract_all_lexers parsed, module_name); 
				    close_out out_channel
				);
				Format.printf "Copying coq files@.";
				file_copy (Filename.concat prog_dirname "LexerDefinition.vo") (Filename.concat inputfile_dirname "LexerDefinition.vo");
				file_copy (Filename.concat prog_dirname "CoqLexUtils.vo") (Filename.concat inputfile_dirname "CoqLexUtils.vo");
				file_copy (Filename.concat prog_dirname "RValues.vo") (Filename.concat inputfile_dirname "RValues.vo");
				file_copy (Filename.concat prog_dirname "MatchLenSimpl.vo") (Filename.concat inputfile_dirname "MatchLenSimpl.vo");
				file_copy (Filename.concat prog_dirname "MatchLen.vo") (Filename.concat inputfile_dirname "MatchLen.vo");
				file_copy (Filename.concat prog_dirname "ShortestLenSimpl.vo") (Filename.concat inputfile_dirname "ShortestLenSimpl.vo");
				file_copy (Filename.concat prog_dirname "ShortestLen.vo") (Filename.concat inputfile_dirname "ShortestLen.vo");
				file_copy (Filename.concat prog_dirname "RegexpSimpl.vo") (Filename.concat inputfile_dirname "RegexpSimpl.vo");
				file_copy (Filename.concat prog_dirname "SubLexeme.vo") (Filename.concat inputfile_dirname "SubLexeme.vo");
				let inputfile_lib_dirname = Filename.concat inputfile_dirname "RegExpLib" in
				create_directory inputfile_lib_dirname;
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Boolean.vo") (Filename.concat inputfile_lib_dirname "Boolean.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Concat.vo") (Filename.concat inputfile_lib_dirname "Concat.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Includes.vo") (Filename.concat inputfile_lib_dirname "Includes.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Star.vo") (Filename.concat inputfile_lib_dirname "Star.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Char.vo") (Filename.concat inputfile_lib_dirname "Char.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Definitions.vo") (Filename.concat inputfile_lib_dirname "Definitions.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "RegExp.vo") (Filename.concat inputfile_lib_dirname "RegExp.vo");
				file_copy (Filename.concat (Filename.concat prog_dirname "regexp_opt") "Utility.vo") (Filename.concat inputfile_lib_dirname "Utility.vo");
				Format.printf "-----------------end of log@.";
				Format.printf "The lexer generation has been successfully executed in %s@." inputfile_dirname; 
				if not !ignore_main then
				    Format.printf "Now go to %s and execute compile.sh@." inputfile_dirname
    end
