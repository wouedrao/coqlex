# CoqLex

This archive contains source code, data and proof of correctness of Coqlex, a formally verified lexer generator described in the paper.

## Prerequisites
Coqlex builds on the following dependencies:

- OCaml = 4.14.0
- Coq = 8.15.2
- Menhir = 20220210
- coq-menhirlib = 20220210 

The coq-menhirlib package can be found in INRIA OPAM repo (https://coq.inria.fr/opam/released). To install it, users must execute the following commands: 
> `opam repo add coq-released https://coq.inria.fr/opam/released`

> `opam update`

> `opam install coq-menhirlib.20220210`






## Build & Usage
To compile, use the makefile and the following command:

> `make`

This command generates a executable called `generator` that is the lexer-generator.

Using a `.vl` file, the lexer generator can produce a lexer written in Coq. To do generate a lexer from the file `file.vl` located in `dir`, run the following command :

> `generator dir/file.vl`

This command generates a directory that containing the directory `CoqLexerGeneratedForfile` in the director `dir`. This directory contains:
- file.v : the Coq code of the lexer.
- Lib.ml : the OCaml code that will facilitate the use of the OCaml code that will be extracted from `file.v`.
- main.ml : the OCaml code that can be used to compute the execution time of a lexical analysis.
- `.vo files` : compiled Coq code that is necessary for `file.v`.
- compile.sh : a script that can be used to extract the OCaml code from `file.v` and other `.vo` files, and produce an executable called `analyzer`. 

`analyzer` is a program that takes the path to a file as input, performs the lexical analysis of that file and prints the execution that of that lexical analysis. This command is used as follows:

> `analyzer -lexer lexer-name file_to_analyse`

As a `.vl` file can be used to define more that one lexer, the `analyzer` requires to specify the name of the lexer to use. 

**Note:** One can refuse to generate the `main.ml` and the `compile.sh` using `generator` with the option `-bench none`. When `main.ml` is generated, lexers are called with `[]` as storage. The user can change that value by modifying `main.ml`. 

### Examples

When those lexers are generated, one can test them using example files located in `example/data-for-lexing`. The `.json` files are used for JSON lexers and the `.ml` files are used for mini-ml example. The `example/data-for-lexing/looping-terminates.txt` and `example/data-for-lexing/looping-diverges.txt` are examples for the looping lexers example. This command calls the generator on `example/loopingLexer.vl`. The trailler of this file contains the proof that the lexer `my_lexer` loops when the input string starts by a character that is different from `a` and `b`. 

We defined 3 lexers to process:
- [JSON](https://medium.com/@aleksandrasays/tutorial-parsing-json-with-ocaml-579cc054924f)

The OCaml and Coq JSON lexers can be generated using the command 

> `make example_json`  

This command generates two executables: `example/CoqLexerGeneratedForJSON/analyzer` and `example/ocamllex-examples/JSON/analyzer`. One can use those executables with the option `-lexer read` and the `.json` files located in `example/data-for-lexing`. This command calls the generator on `example/json.vl`. 

- [Mini-ML](https://github.com/rinderknecht/Mini-ML/blob/master/Lang0/Lexer.mll)

The OCaml and Coq mini-ml lexers can be generated using the command 

> `make example_miniml`

This command generates two executables: `example/CoqLexerGeneratedForMiniml/analyzer` and `example/ocamllex-examples/MINI-ML/analyzer`. One can use those executables with the option `-lexer token_lexer` and the `.ml` files located in `example/data-for-lexing`. This command calls the generator on `example/miniml.vl`.


- Looping lexer (that example can be seen in the paper)

The OCaml and Coq lexers for looping example can be generated using the command

> `make example_loop`

This command generates two executables: `example/CoqLexerGeneratedForLoopingLexer/analyzer` and `example/ocamllex-examples/LoopingLexer/analyzer`. One can use those executables with the option `-lexer my_lexer` and the files `looping-terminates.txt` and `looping-diverges.txt` (looping case) located in `example/data-for-lexing`. This command calls the generator on `example/loopingLexer.vl`.

## Verbatim++ Vs Ocamllex Vs Coqlex
The directory `Comparison` contains: 
- the Verbatim++ JSON benchmark dataset (`Comparison/JSON/data`)
- the Verbatim++ JSON lexer OCaml code (`Comparison/JSON/Lexers/Verbatim`)
- the translation of the Verbatim++ JSON lexer for OCamllex (`Comparison/JSON/Lexers/ocamllex`)
- the translation of the Verbatim++ JSON lexer for Coqlex (`Comparison/JSON/Lexers/json_lexer.vl` and `Comparison/JSON/Lexers/CoqLex`)
- a python script to produce the performance figure presented in the paper (`Comparison/plot.py`)
- a bash script to compile and run all those paper (`Comparison/plot.sh`)

To run the comparison, one can run

> `make compare_json`

This command will perform the comparison on the benchmark located in `Comparison/JSON/data` and then plot the comparison figure `compare.png` in `Comparison/JSON`.


## Implementation details
- `MachLen.v` contains the implementation and the proofs for score computation for the longest match rule.
- `MachLenSimpl.v` contains the optimization of `MachLen.v`.
- `ShortestLen.v` contains the implementation and the proofs for score computation for the shortest match rule.
- `ShortestSimpl.v` contains the optimization of `Shortest.v`.
- `regexp_opt` contains the definitions for Brzozowski derivatives based regexp.
- `RegexpSimpl.v` contains the implementation and the proofs for regexp simplification.
- `RValues.v` contains the implementation of usual regexps.
- `LexerDefinition.v` contains the definitions, the implementations and proofs for election, action.
- `CoqLexUtils.v` contains the definitions of usual action.
- `Extraction.v` containst the Coq instruction for extraction.
- `CoqLexLexer.v` contains the definition of Coqlex lexer used to build the lexer-generator.
- `Parser.vy` contains the definition of Coqlex parser used to build the lexer-generator.
- `lexerUtils.ml` and `parserUtils.ml` are helper files that help to use to manage lexbufs.
- `coqlex.ml` is the entrypoint of the lexer-generator. It reads the input file and handle generations.




