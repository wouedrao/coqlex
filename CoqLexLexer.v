(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex generator lexer definition *)

Add LoadPath "regexp_opt" as RegExp.
Require Import LexerDefinition RValues.
Require Import Parser.
Require Import RegExp.Definitions RegExp.Char.
Require Import CoqLexUtils Ascii String.
Import ListNotations.
Local Open Scope string_scope.


Fixpoint brace_content fuel mem lexbuf {struct fuel} := match fuel with
| 0 => AnalysisNoFuel lexbuf
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re "{", 
            (fun lexbuf => 
                match brace_content n "{" lexbuf with
                | (AnalysisTerminated (Some (BRACECONTENT mem')) lexbuf') => brace_content n (String.append (String.append mem mem') "}") lexbuf'
                | (AnalysisTerminated _ lexbuf') => AnalysisFailedUserRaisedError "Un-expected result" lexbuf'
                | r => r 
                end
            )
          );
          (string_to_re "}", ret_token (BRACECONTENT mem) ); 
          (Char "010"%char, 
            (fun lexbuf => 
                brace_content n (String.append mem (String "010"%char EmptyString)) (new_line_f lexbuf)
            ) 
          ); 
          (plus (charExceptList_to_re [ "{"%char; "}"%char; "010"%char ]), 
            (fun lexbuf => 
                brace_content n (String.append mem lexbuf.(lexeme)) lexbuf
            )
          )
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished BRACECONTENT")
        ]
      ) (remaining_str lexbuf) with
    | Some elt => exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end.


Fixpoint part_content fuel mem lexbuf {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re "%{", 
            (fun lexbuf => 
                match part_content n "%{" lexbuf with
                | (AnalysisTerminated (Some (PARTCONTENT mem')) lexbuf') => part_content n (String.append (String.append mem mem') "}%") lexbuf'
                | (AnalysisTerminated _ lexbuf') => AnalysisFailedUserRaisedError "Un-expected result" lexbuf'
                | r => r 
                end
            )
          );
          (string_to_re "}%", ret_token (PARTCONTENT mem) ); 
          (Char "010"%char, 
            (fun lexbuf => 
                part_content n (String.append mem (String "010"%char EmptyString)) (new_line_f lexbuf)
            ) 
          ); 
          (plus (charExceptList_to_re [ "%"%char; "}"%char; "010"%char ]), 
            (fun lexbuf => 
                part_content n (String.append mem lexbuf.(lexeme)) lexbuf
            )
          );
          (regex_any, 
            (fun lexbuf => 
                part_content n (String.append mem lexbuf.(lexeme)) lexbuf
            )
          )
        ],
        [
          (CoqLexUtils.EOF, raise "Un-finished PARTCONTENT"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some elt => exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end. 

(* Compute (part_content 50 "" (lexbuf_from_string "qsqfqfdsf%}%qsdqsd")).  *)

Fixpoint parent_content fuel mem lexbuf {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (string_to_re "%(", 
            (fun lexbuf => 
                match parent_content n "%(" lexbuf with
                | (AnalysisTerminated (Some (PARENTCONTENT mem')) lexbuf') => parent_content n (String.append (String.append mem mem') ")%") lexbuf'
                | (AnalysisTerminated _ lexbuf') => AnalysisFailedUserRaisedError "Un-expected result" lexbuf'
                | r => r 
                end
            )
          );
          (string_to_re ")%", ret_token (PARENTCONTENT mem) ); 
          (Char "010"%char, 
            (fun lexbuf => 
                parent_content n (String.append mem (String "010"%char EmptyString)) (new_line_f lexbuf)
            ) 
          ); 
          (plus (charExceptList_to_re [ "%"%char; ")"%char; "010"%char ]), 
            (fun lexbuf => 
                parent_content n (String.append mem lexbuf.(lexeme)) lexbuf
            )
          );
          (regex_any, 
            (fun lexbuf => 
                parent_content n (String.append mem lexbuf.(lexeme)) lexbuf
            )
          )
        ],
        [
          (CoqLexUtils.EOF, raise "Un-finished PARENTCONTENT"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some elt => exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end. 

(* Compute (parent_content 50 "" (lexbuf_from_string "()()%")). *)

Fixpoint comment {Token: Set} fuel lexbuf {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf)
| S n =>
  (  
    match generalizing_elector longest_match_elector (Action := semantic_action (Token:=Token))
      (
        [
          (string_to_re "(*", sequence [comment n; comment n]); 
          (string_to_re "*)", ret_nothing); 
          (Char "010", sequence [new_line; comment n]); 
          (plus (charExceptList_to_re [ "("%char; "*"%char; "010"%char ]), comment n);
          (regex_any, comment n)
        ], 
        [
          (CoqLexUtils.EOF, raise "Un-finished Comment"%string)
        ]
      ) 
      (remaining_str lexbuf) with
    | Some elt => exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end. 

Fixpoint vlllexer fuel lexbuf {struct fuel} := match fuel with
| 0 => (AnalysisNoFuel lexbuf)
| S n =>
  (  
    match generalizing_elector longest_match_elector 
      (
        [
          (charList_to_re [" "%char; "009"%char; "013"%char], vlllexer n); 
          (Char "010", sequence [new_line; vlllexer n]);
			    (Char "(", ret_token (LPARENT tt)); 
			    (Char ")", ret_token (RPARENT tt)); 
			    (Char "[", ret_token (LBRACKET tt)); 
			    (Char "]", ret_token (RBRACKET tt)); 
			    (Char "*", ret_token (STAR tt)); 
			    (Char "_", ret_token (UNDERSCORE tt)); 
			    (Char "+", ret_token (PLUS tt)); 
			    (Char "|", ret_token (VBAR tt)); 
			    (Char "?", ret_token (QMARK tt)); 
			    (Char "-", ret_token (MINUS tt)); 
			    (Char "^", ret_token (CIRCUMFLEX tt)); 
			    (Char "$", ret_token (DOLLARD tt)); 
      		(string_to_re "%polymorphic_token", ret_token (TOKEN_POLY tt)); 
      		(string_to_re "EOF", ret_token (EOF_SYMB tt)); 
      		(string_to_re "eof", ret_token (EOF_SYMB tt)); 
      		(Char "=", ret_token (EQ tt)); 
          (string_to_re "let", ret_token (LET tt)); 
			    (string_to_re "rule", ret_token (RULE tt)); 
			    (string_to_re "as", ret_token (AS tt)); 
			    (string_to_re "and", ret_token (AND tt)); 
			    (string_to_re "then", ret_token (THEN tt)); 
			    (string_to_re "parse", ret_token (PARSE tt)); 
			    (string_to_re "shortest", ret_token (SHORTEST tt)); 
      		(identifier, ret_lexeme IDENT); 
      		(string_regex, ret_lexeme (fun str => STR_LIT (ocamlStrInterp (removeFirstAndLast str))) ); 
      		(char_regex, 
      		  (fun lexbuf => 
      		    match to_Char (ocamlStrInterp (removeFirstAndLast lexbuf.(lexeme))) with 
      		    | Some v => (AnalysisTerminated (Some (CH_LIT v)) lexbuf)
      		    | None => (AnalysisFailedUserRaisedError "Impossible" lexbuf) 
      		    end
    		    )
  		    ); 
      		(string_to_re "(*", sequence [comment (fuel * 5000); vlllexer n]); 
          (string_to_re "{", brace_content (fuel * 5000) "");
          (string_to_re "%{", brace_content (fuel * 5000) "");
      		(string_to_re "%(", (parent_content (fuel * 5000)) "")
        ], 
        [
          (CoqLexUtils.EOF, ret (Parser.EOF tt))
        ]
      ) 
      (remaining_str lexbuf) with
    | Some elt => exec_sem_action elt lexbuf
    | None => (AnalysisFailedEmptyToken lexbuf)
    end
  )
end. 


