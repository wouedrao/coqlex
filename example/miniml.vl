(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*             (c) 2021 Siemens Mobility SAS and Inria Saclay          *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

{
Require Import String.
Inductive t_type := 
    | LET : t_type
    | REC : t_type
    | AND : t_type
    | IN : t_type
    | FUN : t_type
    | If : t_type
    | THEN : t_type
    | ELSE : t_type
    | TRUE : t_type
    | FALSE : t_type
    | NOT : t_type
    | COMMA : t_type
    | EQ : t_type
    | NE : t_type   
    | LE : t_type   
    | GE : t_type   
    | LT : t_type   
    | GT : t_type   
    | ARROW : t_type
    | MINUS : t_type
    | PLUS : t_type 
    | DIV : t_type  
    | MULT : t_type 
    | BOOL_AND : t_type
    | BOOL_OR : t_type
    | LPAR : t_type 
    | RPAR : t_type 
    | STR : string -> t_type 
    | Nat : string -> t_type 
    | Ident : string -> t_type 
    | EOF : t_type . 
    
    Definition keywords :=
   [ ("let"%string,   LET);
          ("rec"%string,   REC);
          ("and"%string,   AND);
          ("in"%string,    IN);
          ("fun"%string,   FUN);
          ("if"%string,    If);
          ("then"%string,  THEN);
          ("else"%string,  ELSE);
          ("true"%string,  TRUE);
          ("false"%string, FALSE);
          ("not"%string,   NOT)].

Parameter list_assoc_opt : string -> list (string * t_type) -> option t_type.

Extract Inlined Constant list_assoc_opt => "List.assoc_opt".

Definition comment_f_store lexbuf storage := 
    (lexbuf.(s_pos), lexbuf.(cur_pos))::storage.
    
}

let newline_regex    = '\n' | '\r' | "\r\n"
let blank      = ' ' | '\t'
let lowercase  = ['a'-'z' '_']
let uppercase  = ['A'-'Z' '_']
let ident_char = ['A'-'Z' 'a'-'z' '_' '\'' '0'-'9']
let ident      = lowercase ident_char*
let constr     = uppercase ident_char*
let nat        = ['1'-'9'] ['0'-'9']* | '0'
let escaped    = "\\n" | "\\\"" | "\\?" | "\\\\" | "\\a" | "\\b" | "\\f" | "\\r" | "\\t" | "\\v"
let str_char   = [^'"' '\\' '\n'] | escaped
let string_regex     = '"' str_char* '"'

rule comment mem = parse
  "(*"         { sequence [comment ((lexbuf.(s_pos), lexbuf.(cur_pos))::mem); comment mem]}
| "*)"         { fun lexbuf => match mem with
                    | [] => raise "Call of comment without args"%string lexbuf
                    | h::t => (AnalysisTerminated None lexbuf)
                    end }
| newline_regex    { sequence [ new_line; comment mem] }
| _            { comment mem }
| EOF          { raise "Opened comment."%string }
%polymorphic_token

then rule token_lexer = parse
  newline_regex      { sequence [ new_line; token_lexer] }
| blank+       { token_lexer }
| ","          { ret COMMA                                }
| "="          { ret EQ                                   }
| "<>"         { ret NE                                   }
| "=<"         { ret LE                                   }
| ">="         { ret GE                                   }
| "<"          { ret LT                                   }
| ">"          { ret GT                                   }
| "->"         { ret ARROW                                }
| "-"          { ret MINUS                                }
| "+"          { ret PLUS                                 }
| "/"          { ret DIV                                  }
| "*"          { ret MULT                                 }
| "&&"         { ret BOOL_AND                             }
| "||"         { ret BOOL_OR                              }
| "("          { ret LPAR                                 }
| ")"          { ret RPAR                                 }
| string_regex { ret_l STR  }
| "(*"         { 
                 sequence [ comment [(lexbuf.(s_pos), lexbuf.(cur_pos))]; token_lexer ]
               }
| nat          { ret_l Nat }
| ident        { 
                    fun lexbuf => 
                        match list_assoc_opt lexbuf.(lexeme) keywords with
                        | Some t => ret t lexbuf
                        | None => ret (Ident lexbuf.(lexeme)) lexbuf
                        end
               }
               
| _       { 
                raise_l (String.append "Invalid character "%string)
               }
| EOF          { ret EOF }
