(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*             (c) 2022 Siemens Mobility SAS and Inria Saclay          *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique. All rights reserved. This file is distributed under   *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

rule my_lexer = parse
    |  'b' 'a'* 'b' { ret 0 }
    |  'a'* { my_lexer }
    | EOF { ret 1 }
    
{
Require Import MatchLen MatchLenSimpl.
Opaque ascii_dec.

Require Import MatchLen MatchLenSimpl.
Opaque ascii_dec.

Lemma my_lexer_loops_election : forall sh st n,
  sh <> "a"%char -> sh <> "b"%char -> 
  generalizing_elector (Action := semantic_action) LexerDefinition.longest_match_elector ([
        (Cat (Char  "b"%char ) (Cat (Star (Char  "a"%char )) (Char  "b"%char )),  ret 0 );
        (Star (Char  "a"%char ),  (my_lexer n) )],
        [(CoqLexUtils.EOF,  ret 1 )]
        ) (String sh st) = Some (RegexpElected (mkRegexpElectionResult (semantic_action ) (Star (Char "097"%char)) (my_lexer n) 0 "" (String sh st))).
Proof.
intros.
simpl.
case_eq (ascii_dec "b" sh).
intros.
contradict H0.
apply eq_sym; auto.
intros.
simpl.
assert (match_len_with_lexeme Empty st = None).
apply match_len_with_lexeme_none.
rewrite match_len_correct.
apply match_len_empty.
rewrite H2.
case_eq (ascii_dec "a" sh).
intros.
contradict H.
apply eq_sym; auto.
intros.
simpl.
rewrite H2.
auto.
Qed.

Opaque generalizing_elector.

Theorem my_lexer_loops: forall sh st, sh <> "a"%char -> sh <> "b"%char -> forall fuel lexbuf,
   lexbuf.(remaining_str) = String sh st -> exists lexbuf',
   my_lexer fuel lexbuf = (AnalysisNoFuel lexbuf').
Proof.
induction fuel; simpl; eauto.
unfold exec_sem_action.
intros.
rewrite H1.
rewrite my_lexer_loops_election; auto.
simpl.
eauto.
Qed.
}
