{
    open Buffer
    
    type t_type = 
        | INT of string 
        | FLOAT of string 
        | STRING of string
        | TRUE 
        | FALSE 
        | NULL 
        | LEFT_BRACE 
        | RIGHT_BRACE 
        | LEFT_BRACK 
        | RIGHT_BRACK 
        | COLON 
        | COMMA
        | EOF ;;
}

let int_regex = '-'? ['0'-'9'] ['0'-'9']*
let digit = ['0'-'9']
let frac = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float_regex = digit* frac? exp?
let white = [' ' '\t']+
let newline_regex = '\r' | '\n' | "\r\n"
let id = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

rule read_string buff =
  parse
  | '"'       { 
                STRING (Buffer.contents buff)
              }
  | [^ '"' '\\']+
    { 
        Buffer.add_string buff (Lexing.lexeme lexbuf);
        read_string buff lexbuf
    }
  
  | eof {
        failwith "EOF while read_string."
    }
    
and read =
  parse
  | white    {  read lexbuf }
  | newline_regex  { Lexing.new_line lexbuf; read lexbuf }
  | int_regex      { INT (Lexing.lexeme lexbuf) }
  | float_regex    { FLOAT (Lexing.lexeme lexbuf) }
  | "true"   { TRUE }
  | "false"  { FALSE }
  | "null"   { NULL }
  | '"'      { read_string (Buffer.create 20) lexbuf }
  | '{'      { LEFT_BRACE }
  | '}'      { RIGHT_BRACE }
  | '['      { LEFT_BRACK }
  | ']'      { RIGHT_BRACK }
  | ':'      { COLON }
  | ','      { COMMA }
  | _ { failwith ("Unexpected char: " ^ (Lexing.lexeme lexbuf)) }
  | eof      { EOF }
