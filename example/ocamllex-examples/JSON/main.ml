open Format
open Lexing
open Json_lexer

let in_channel_to_string in_channel = 
	let rec in_channel_to_string_rec in_channel acc = 
		(
		  match Stdlib.input_line in_channel with
		    | exception End_of_file -> List.rev acc
		    | str -> in_channel_to_string_rec in_channel (str::acc)
		) in
	String.concat "\n" (in_channel_to_string_rec in_channel [])

let filepath_to_string path = let in_channel = Stdlib.open_in path in in_channel_to_string in_channel

let () =
    let usage_msg = Format.sprintf "A program to run a benchmark on a Coq-lexer.
Usage : %s filename -lexer_name string -default_str string@." Sys.argv.(0) in
    let inputPath = ref None in
    let lexer_name = ref "" in
    let speclist = 
        [
            ("-lexer", Arg.Set_string lexer_name, "The name of the lexer to benchmark")
        ] in
    Arg.parse speclist (fun anon -> inputPath := Some anon) usage_msg;
    match !inputPath with
    | None -> failwith "No input file"
    | Some path -> 
        let _ = Format.printf "Reading input file@." in
        let input_string = filepath_to_string path in
        let lexbuf = Lexing.from_string input_string in
        (match !lexer_name with 
            | "read_string" -> 
                let count = ref 0 in
                let rec loop storage lexbuf = 
                (
                    ignore(read_string storage lexbuf);
                    count := !count + 1; 
                    if lexbuf.lex_curr_pos  = lexbuf.lex_buffer_len then ()
                    else (loop storage lexbuf)
                )
                in
                let _ = Format.printf "Starting evaluation of %s on %s@." !lexer_name path in
                let start = Unix.gettimeofday() in
                let _ = loop (Buffer.create 20) lexbuf in
                let end_ = Unix.gettimeofday() in
                Format.printf "Time = %f (for %d tokens)@." (end_ -. start) !count 
            | "read" -> 
                let count = ref 0 in
                let rec loop lexbuf = 
                (
                    ignore(read lexbuf);
                    count := !count + 1; 
                    if lexbuf.lex_curr_pos  = lexbuf.lex_buffer_len then ()
                    else (loop lexbuf)
                )
                in
                let _ = Format.printf "Starting evaluation of %s on %s@." !lexer_name path in
                let start = Unix.gettimeofday() in
                let _ = loop lexbuf in
                let end_ = Unix.gettimeofday() in
                Format.printf "Time = %f (for %d tokens)@." (end_ -. start) !count 
            | _ -> failwith "Lexer not found")
