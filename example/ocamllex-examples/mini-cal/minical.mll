{
open Lexing
open TokenDefinition
}

(* regexp definitions *)
let ident = ['a'-'z']+
let numb = ['0'-'9']+

(* lexer definitions*)
rule minlexer = parse
| '\n' { new_line lexbuf; minlexer lexbuf }
| ident {ID (Lexing.lexeme lexbuf)}
| numb { Number (Lexing.lexeme lexbuf)}
| '+' { PLUS }
| '-' { MINUS }
| '*' { TIMES }
| '(' { LPAREN }
| ')' { RPAREN }
| eof { Eof }
| _ { failwith ("unknown token : " ^ (Lexing.lexeme lexbuf))}

(* trailer section *)
{}
