(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex election and lexbuf definition *)

Add LoadPath "regexp_opt" as RegExp.

Require Import RegExp.Definitions.
Require Import Lia.
Require Import MatchLenSimpl.
Require Import ShortestLenSimpl.
Require Import RegexpSimpl.
Require Import Coq.Lists.List String.
Import ListNotations.
Require Import Recdef Coq.Program.Wf.

Record Position : Set := mkPosition
{
  l_number : nat;
  c_number : nat;
  abs_pos : nat
}.

Definition OPos := {| l_number := 1;  c_number := 0;  abs_pos := 0 |}.

Record Lexbuf : Set := mkLexbuf
{
  s_pos : Position
  ;lexeme : string
  ;cur_pos : Position
  ;remaining_str : string
}.

Definition lexbuf_from_string str := {|
           s_pos := OPos;
           lexeme := EmptyString;
           cur_pos := OPos;
           remaining_str := str |}.

Inductive LexicalAnalysisResult {Token : Set} : Set :=
| AnalysisTerminated : option Token -> Lexbuf -> LexicalAnalysisResult
| AnalysisFailedUserRaisedError : string -> Lexbuf -> LexicalAnalysisResult
| AnalysisFailedEmptyToken : Lexbuf -> LexicalAnalysisResult
| AnalysisNoFuel : Lexbuf -> LexicalAnalysisResult
| SubLexemeError : string -> Lexbuf -> LexicalAnalysisResult
.

(* Election... *)

Record RegexpElectionResult {Action : Set} : Set := mkRegexpElectionResult
{
   electionRegexp : RegExp
  ;electionAction : Action
  ;electionScore : nat
  ;matched: string
  ;suffix: string
}.

(* String Utils *)
Fixpoint string_split n str := match str with
| EmptyString => (EmptyString, EmptyString)
| String h t => 
  (
    match n with
    | 0 => (EmptyString, String h t)
    | S n' => 
      (
        let (l, r) := string_split n' t in
        (String h l, r)
      )
    end
  )
end.

Lemma string_split_concat : forall n str l r, string_split n str = (l, r) -> append l r = str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

Lemma string_split_left : forall n str l r, string_split n str = (l, r) -> l = substring 0 n str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

Lemma string_split_right : forall n str l r, string_split n str = (l, r) -> r = substring n (length str - n) str.
Proof.
induction n, str; simpl; intros.
inversion H; subst; auto.
inversion H; subst; auto.
f_equal.
induction str; simpl; auto.
f_equal.
auto.
inversion H; subst; auto.
case_eq (string_split n str); intros; rewrite H0 in *.
apply IHn in H0.
inversion H; subst.
auto.
Qed.

(** Longest match correctness **)

Fixpoint match_len_with_lexeme re s := 
match re with
| Eps => Some (0, EmptyString, s)
| Empty => None
| _ => ( match s with
        | EmptyString => if nu re then Some (0, EmptyString, EmptyString) else None
        | String a s2 => (match match_len_with_lexeme (RegexpSimpl.derive a re) s2 with
                          | Some (score, lex, suf) => Some ((S score), (String a lex), suf) 
                          | None => if nu re then Some (0, EmptyString, s) else None
                          end)
        end )
end.

Lemma match_len_with_lexeme_none: forall s re, 
  match_len_with_lexeme re s = None <-> 
  MatchLenSimpl.match_len re s = None.
Proof.
induction s; simpl; intros; induction re; try solve [split; intros; inversion H; auto]. 
induction (nu (Cat re1 re2)); split; intros; auto; inversion H.
induction (nu (re1 || re2)); split; intros; auto; inversion H.
induction (nu (And re1 re2)); split; intros; auto; inversion H.
induction (nu (Minus re1 re2)); split; intros; auto; inversion H.
split; intros; [case_eq (match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H; [induction p, a1; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (Char a0)); [inversion H|auto]| case_eq (match_len (derive a (Char a0)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (Char a0)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (Cat re1 re2)) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (Cat re1 re2)); [inversion H|auto]| case_eq (match_len (derive a (Cat re1 re2)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (Cat re1 re2)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (re1 || re2)) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (re1 || re2)); [inversion H|auto]| case_eq (match_len (derive a (re1 || re2)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (re1 || re2)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (Star re)) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (Star re)); [inversion H|auto]| case_eq (match_len (derive a (Star re)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (Star re)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (And re1 re2)) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (And re1 re2)); [inversion H|auto]| case_eq (match_len (derive a (And re1 re2)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (And re1 re2)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a AnyChar) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu AnyChar); [inversion H|auto]| case_eq (match_len (derive a AnyChar) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu AnyChar); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H; [induction p, a1; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (CharExcept a0)); [inversion H|auto]| case_eq (match_len (derive a (CharExcept a0)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (CharExcept a0)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H; [induction p, a2; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (CharRange a0 a1)); [inversion H|auto]| case_eq (match_len (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (CharRange a0 a1)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H; [induction p, a2; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (CharRangeExcept a0 a1)); [inversion H|auto]| case_eq (match_len (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (CharRangeExcept a0 a1)); [inversion H|auto]].
split; intros; [case_eq (match_len_with_lexeme (derive a (Minus re1 re2)) s); intros; rewrite H0 in H; [induction p, a0; inversion H|apply IHs in H0; rewrite H0]
  ; induction (nu (Minus re1 re2)); [inversion H|auto]| case_eq (match_len (derive a (Minus re1 re2)) s); intros; rewrite H0 in H; [inversion H| apply IHs in H0; rewrite H0]
  ; induction (nu (Minus re1 re2)); [inversion H|auto]].
Qed.  
  

Theorem match_len_with_lexeme_detail_score: forall s re score matched suff, 
  match_len_with_lexeme re s = Some (score, matched, suff) -> 
  MatchLenSimpl.match_len re s = Some score.
Proof.
induction s; simpl; intros; induction re; try solve [inversion H; auto].
induction (nu (Cat re1 re2)); inversion H; auto.
induction (nu (re1 || re2)); inversion H; auto.
induction (nu (And re1 re2)); inversion H; auto.
induction (nu (Minus re1 re2)); inversion H; auto.
case_eq (match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H; [|inversion H]; induction p; induction a1
;inversion H; apply IHs in H0; rewrite H0; auto
.
case_eq (match_len_with_lexeme (derive a (Cat re1 re2)) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (Cat re1 re2)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (re1 || re2)) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (re1 || re2)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (Star re)) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (Star re)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (And re1 re2)) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (And re1 re2)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a AnyChar) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu AnyChar); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H; [|inversion H]; induction p; induction a1
;inversion H; apply IHs in H0; rewrite H0; auto.
case_eq (match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H; 
  [induction p; induction a2;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (CharRange a0 a1)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H; 
  [induction p; induction a2;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (CharRangeExcept a0 a1)); inversion H; auto].
case_eq (match_len_with_lexeme (derive a (Minus re1 re2)) s); intros; rewrite H0 in H; 
  [induction p; induction a0;inversion H; apply IHs in H0; rewrite H0; auto
    |apply match_len_with_lexeme_none in H0; rewrite H0; induction (nu (Minus re1 re2)); inversion H; auto]. 
Qed.

Theorem match_len_with_lexeme_detail_str: forall s re score matched suff, 
  match_len_with_lexeme re s = Some (score, matched, suff) -> 
  string_split score s = (matched, suff).
Proof.
induction s; simpl.
induction re; intros; try solve [inversion H].
inversion H; simpl; auto.
induction (nu (Cat re1 re2)); inversion H; simpl; auto.
induction (nu (re1 || re2)); inversion H; simpl; auto.
induction (nu (Star re)); inversion H; simpl; auto.
induction (nu (And re1 re2)); inversion H; simpl; auto.
induction (nu (Minus re1 re2)); inversion H; simpl; auto.
induction re; intros; try solve [inversion H].
inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H; 
  [induction p, a1; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (Char a0)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (Cat re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (Cat re1 re2)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (re1 || re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (re1 || re2)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (Star re)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (Star re)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (And re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (And re1 re2)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a AnyChar) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu AnyChar); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H; 
  [induction p, a1; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (Char a0)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H; 
  [induction p, a2; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (CharRange a0 a1)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H; 
  [induction p, a2; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (CharRangeExcept a0 a1)); inversion H; simpl; auto.
case_eq (match_len_with_lexeme (derive a (Minus re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H; subst; apply IHs in H0; simpl; rewrite H0; auto|]
; induction (nu (Minus re1 re2)); inversion H; simpl; auto.
Qed.
  

Fixpoint longest_match_elector {Action : Set} lst str := match lst with
| [] => None
| (re, ac)::t => 
  (
    match match_len_with_lexeme re str with
    | None => longest_match_elector t str
    | Some (n, lex, suf) => 
        Some (
          match longest_match_elector t str with
          | None => (mkRegexpElectionResult Action re ac n lex suf)
          | Some candidate => (if Nat.ltb n (electionScore candidate) then candidate else (mkRegexpElectionResult Action re ac n lex suf) )
          end
        )
    end
  )
end.

(*** longest_match_elector properties ***)

Lemma longest_match_elector_None {Action : Set}: forall l str, longest_match_elector (Action:=Action) l str = None <-> (forall re ac, In (re, ac) l -> match_len re str = None).
Proof.
induction l; simpl.
split; intros; auto.
inversion H0.
induction a.
intro.
case_eq (match_len_with_lexeme a str).
induction p, a0.
split.
intros.
inversion H0.
intros.
assert ((a, b) = (a, b) \/ In (a, b) l).
left; auto.
assert (D := H0 a b H1).
apply match_len_with_lexeme_detail_score in H.
rewrite H in D.
inversion D.
split.
apply match_len_with_lexeme_none in H.
intros.
inversion H1.
inversion H2; subst; auto.
assert (D := IHl str).
inversion D.
eapply H3 in H0; eauto.
intros.
apply IHl.
intros.
eapply H0; eauto.
Qed.

Lemma longest_match_elector_max {Action : Set}: forall l str h, longest_match_elector (Action:=Action) l str = Some h -> (forall re ac n, In (re, ac) l -> match_len re str = Some n -> (electionScore h) >= n).
Proof.
induction l; simpl.
intros.
inversion H.
induction a.
intros str h.
case_eq (match_len_with_lexeme a str); intros n H.
induction n, a0.
apply match_len_with_lexeme_detail_score in H.
case_eq (longest_match_elector l str); intros r H0.
case_eq (Nat.ltb n (electionScore r)); intros.
inversion H2; subst.
inversion H3.
inversion H5; subst.
rewrite H in H4; inversion H4; subst.
apply PeanoNat.Nat.ltb_lt in H1; lia.
eapply IHl; eauto.
inversion H2; subst.
simpl.
inversion H3.
inversion H5; subst.
rewrite H in H4; inversion H4; subst; auto.
assert (D := IHl str r H0 re ac n0 H5 H4).
apply PeanoNat.Nat.ltb_ge in H1; lia.
intros.
inversion H1.
inversion H3; subst.
rewrite H in H2; inversion H2; subst.
inversion H0; subst.
simpl.
auto.
assert (D := longest_match_elector_None (Action := Action) l str).
inversion D.
assert (D' := H4 r re ac H3).
rewrite H2 in D'; inversion D'.
intros.
inversion H0.
inversion H2; subst.
apply match_len_with_lexeme_none in n.
rewrite n in H1; inversion H1.
eauto.
Qed.

Lemma longest_match_elector_priority {Action : Set}: 
  forall l str re ac cand n matched suff, 
    match_len re str = Some n ->
    string_split n str = (matched, suff) ->
    longest_match_elector (Action:=Action) l str = Some cand -> 
    n = (electionScore cand) ->
    longest_match_elector (Action:=Action) ((re, ac)::l) str = Some (mkRegexpElectionResult Action re ac n matched suff).
Proof.
intros.
simpl.
case_eq (match_len_with_lexeme re str); intros; 
  [assert (H3':=H3); induction p, a; apply match_len_with_lexeme_detail_score in H3|
  apply match_len_with_lexeme_none in H3; rewrite H in H3; inversion H3].
rewrite H in H3; inversion H3; subst.
apply match_len_with_lexeme_detail_str in H3'.
rewrite H0 in H3'; inversion H3'; subst.
assert (Nat.ltb (electionScore cand) (electionScore cand) = false).
apply PeanoNat.Nat.ltb_ge.
lia.
rewrite H1.
rewrite H2.
auto.
Qed.

(** Shortest match elector*)
Fixpoint shortest_match_len_with_lexeme re s := 
match re with
| Empty => None
| Eps => Some (0, EmptyString, s)
| Star _ => Some (0, EmptyString, s)
| _ => 
(
  match s with
  | EmptyString => if nu re then Some (0, EmptyString, s) else None
  | String a s2 => if nu re then Some (0, EmptyString, s) else (match shortest_match_len_with_lexeme (RegexpSimpl.derive a re) s2 with
                  | Some (score, lex, suf) => Some ((S score), (String a lex), suf) 
                  | None => None
                  end)
  end
)
end.

Lemma shortest_match_len_with_lexeme_none: forall s re, 
  shortest_match_len_with_lexeme re s = None <-> 
  ShortestLenSimpl.shortest_match_len re s = None.
Proof.
induction s; simpl; intros; induction re; try solve [split; intros; inversion H; auto]. 
induction (nu (Cat re1 re2)); split; intros; auto; inversion H.
induction (nu (re1 || re2)); split; intros; auto; inversion H.
induction (nu (And re1 re2)); split; intros; auto; inversion H.
induction (nu (Minus re1 re2)); split; intros; auto; inversion H.
induction (nu (Char a0)); [split; intros; inversion H|]; 
split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H; 
  [induction p, a1; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (Char a0)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (Cat re1 re2)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (Cat re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (Cat re1 re2)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (re1 || re2)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (re1 || re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (re1 || re2)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (And re1 re2)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (And re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (And re1 re2)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu AnyChar); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a AnyChar) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a AnyChar) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H; 
  [induction p, a1; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (CharExcept a0)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (CharRange a0 a1)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H; 
  [induction p, a2; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (CharRange a0 a1)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (CharRangeExcept a0 a1)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H; 
  [induction p, a2; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (CharRangeExcept a0 a1)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
induction (nu (Minus re1 re2)); [split; intros; inversion H|]; split; intros; [case_eq (shortest_match_len_with_lexeme (derive a (Minus re1 re2)) s); intros; rewrite H0 in H; 
  [induction p, a0; inversion H|apply IHs in H0; rewrite H0; auto]|]; case_eq (shortest_match_len (derive a (Minus re1 re2)) s); intros;
  rewrite H0 in H; [inversion H|]; apply IHs in H0; rewrite H0; auto.
Qed.

Theorem shortest_match_len_with_lexeme_detail_score: forall s re score matched suff, 
  shortest_match_len_with_lexeme re s = Some (score, matched, suff) -> 
  ShortestLenSimpl.shortest_match_len re s = Some score.
Proof.
induction s; simpl; intros; induction re; try solve [inversion H; auto].
induction (nu (Cat re1 re2)); inversion H; auto.
induction (nu (re1 || re2)); inversion H; auto.
induction (nu (And re1 re2)); inversion H; auto.
induction (nu (Minus re1 re2)); inversion H; auto.
case_eq (shortest_match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H; [|inversion H]; induction p; induction a1
;inversion H; apply IHs in H0; rewrite H0; auto
.
induction (nu (Cat re1 re2)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (Cat re1 re2)) s); intros
; rewrite H0 in H; [induction p, a0|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
induction (nu (re1 || re2)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (re1 || re2)) s); intros
; rewrite H0 in H; [induction p, a0|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
induction (nu (And re1 re2)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (And re1 re2)) s); intros
; rewrite H0 in H; [induction p, a0|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
induction (nu AnyChar); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a AnyChar) s); intros
; rewrite H0 in H; [induction p, a0|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
case_eq (shortest_match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H; [|inversion H]; induction p; induction a1
;inversion H; apply IHs in H0; rewrite H0; auto
.
induction (nu (CharRange a0 a1)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros
; rewrite H0 in H; [induction p, a2|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
induction (nu (CharRangeExcept a0 a1)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros
; rewrite H0 in H; [induction p, a2|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
induction (nu (Minus re1 re2)); [inversion H; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (Minus re1 re2)) s); intros
; rewrite H0 in H; [induction p, a0|]; inversion H; subst; apply IHs in H0; rewrite H0; auto
.
Qed.

Theorem shortest_match_len_with_lexeme_detail_str: forall s re score matched suff, 
  shortest_match_len_with_lexeme re s = Some (score, matched, suff) -> 
  string_split score s = (matched, suff).
Proof.
induction s; simpl.
induction re; intros; try solve [inversion H].
inversion H; simpl; auto.
induction (nu (Cat re1 re2)); inversion H; simpl; auto.
induction (nu (re1 || re2)); inversion H; simpl; auto.
induction (nu (Star re)); inversion H; simpl; auto.
induction (nu (And re1 re2)); inversion H; simpl; auto.
induction (nu (Minus re1 re2)); inversion H; simpl; auto.
induction re; intros; try solve [inversion H].
inversion H; simpl; auto.
induction (nu (Char a0)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (Char a0)) s); intros; rewrite H0 in H;
inversion H; induction p, a1; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (Cat re1 re2)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (Cat re1 re2)) s); intros; rewrite H0 in H;
inversion H; induction p, a0; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (re1 || re2)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (re1 || re2)) s); intros; rewrite H0 in H;
inversion H; induction p, a0; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
inversion H; subst; simpl; auto.
induction (nu (And re1 re2)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (And re1 re2)) s); intros; rewrite H0 in H;
inversion H; induction p, a0; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu AnyChar); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a AnyChar) s); intros; rewrite H0 in H;
inversion H; induction p, a0; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (Char a0)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (CharExcept a0)) s); intros; rewrite H0 in H;
inversion H; induction p, a1; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (CharRange a0 a1)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (CharRange a0 a1)) s); intros; rewrite H0 in H;
inversion H; induction p, a2; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (CharRangeExcept a0 a1)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (CharRangeExcept a0 a1)) s); intros; rewrite H0 in H;
inversion H; induction p, a2; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
induction (nu (Minus re1 re2)); [inversion H; simpl; auto|]; case_eq (shortest_match_len_with_lexeme (derive a (Minus re1 re2)) s); intros; rewrite H0 in H;
inversion H; induction p, a0; apply IHs in H0; simpl; inversion H; subst; simpl; rewrite H0; auto.
Qed.


Fixpoint shortest_match_elector {Action : Set} lst str := match lst with
| [] => None
| (re, ac)::t => 
  (
    match shortest_match_len_with_lexeme re str with
    | None => shortest_match_elector t str
    | Some (n, lex, suff) => 
        Some (
          match shortest_match_elector t str with
          | None => (mkRegexpElectionResult Action re ac n lex suff)
          | Some candidate => (if Nat.leb n (electionScore candidate) then (mkRegexpElectionResult Action re ac n lex suff) else candidate )
          end
        )
    end
  )
end.

(*** shortest_match_elector properties ***)

Lemma shortest_match_elector_None {Action : Set}: forall l str, shortest_match_elector (Action:=Action) l str = None <-> (forall re ac, In (re, ac) l -> shortest_match_len re str = None).
Proof.
induction l; simpl.
split; intros; auto.
inversion H0.
induction a.
intro.
case_eq (shortest_match_len_with_lexeme a str).
induction p, a0.
split.
intros.
inversion H0.
intros.
apply shortest_match_len_with_lexeme_detail_score in H.
assert ((a, b) = (a, b) \/ In (a, b) l).
left; auto.
assert (D := H0 a b H1).
rewrite H in D.
inversion D.
split.
apply shortest_match_len_with_lexeme_none in H.
intros.
inversion H1.
inversion H2; subst; auto.
assert (D := IHl str).
inversion D.
eapply H3 in H0; eauto.
apply shortest_match_len_with_lexeme_none in H.
intros.
apply IHl.
intros.
eapply H0; eauto.
Qed.

Lemma shortest_match_elector_min {Action : Set}: forall l str h, shortest_match_elector (Action:=Action) l str = Some h -> (forall re ac n, In (re, ac) l -> shortest_match_len re str = Some n -> (electionScore h) <= n).
Proof.
induction l; simpl.
intros.
inversion H.
induction a.
intros str h.
case_eq (shortest_match_len_with_lexeme a str).
induction p, a0; intro.
+ case_eq (shortest_match_elector l str); intros.
  case_eq (Nat.leb n (electionScore r)); intros; rewrite H4 in *.
  simpl; apply PeanoNat.Nat.leb_le in H4.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H5; subst.
  apply shortest_match_len_with_lexeme_detail_score in H.
  rewrite H in H3; inversion H3; subst; lia.
  assert (electionScore r <= n0).
  eapply IHl; eauto.
  lia.
  simpl; apply PeanoNat.Nat.leb_gt in H4.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H5; subst.
  apply shortest_match_len_with_lexeme_detail_score in H.
  rewrite H in H3; inversion H3; subst; lia.
  eapply IHl; eauto.
  inversion H1; subst; simpl.
  inversion H2.
  inversion H4; subst.
  apply shortest_match_len_with_lexeme_detail_score in H.
  rewrite H in H3; inversion H3; subst; lia.
  assert (forall (re : RegExp) (ac : Action),
   In (re, ac) l -> shortest_match_len re str = None).
  apply shortest_match_elector_None; auto.
  assert (H' := H5 re ac H4).
  rewrite H3 in H'.
  inversion H'.
+ intros.
  inversion H1.
  inversion H3; subst.
  apply shortest_match_len_with_lexeme_none in H.
  rewrite H in H2.
  inversion H2.
  eapply IHl ; eauto.
Qed.  

Lemma shortest_match_elector_priority {Action : Set}: 
  forall l str re ac cand n matched suff, 
    shortest_match_len re str = Some n ->
    string_split n str = (matched, suff) ->
    shortest_match_elector (Action:=Action) l str = Some cand -> 
    n = (electionScore cand) ->
    shortest_match_elector (Action:=Action) ((re, ac)::l) str = Some (mkRegexpElectionResult Action re ac n matched suff).
Proof.
intros.
simpl.
case_eq (shortest_match_len_with_lexeme re str); intros; [induction p, a; assert (H3':=H3)|apply shortest_match_len_with_lexeme_none in H3; rewrite H in H3; inversion H3].
apply shortest_match_len_with_lexeme_detail_score in H3; rewrite H in H3; inversion H3; subst.
apply shortest_match_len_with_lexeme_detail_str in H3'.
rewrite H0 in H3'; inversion H3'; subst.
rewrite H1.
assert (Nat.leb (electionScore cand) (electionScore cand) = true).
apply PeanoNat.Nat.leb_le.
lia.
rewrite H2.
auto.
Qed.

(** Generalizing elector **)
Inductive electionResult {Action : Set} : Set := 
| RegexpElected : RegexpElectionResult (Action := Action) -> electionResult
| FunctionElected : (string -> bool) -> Action -> electionResult.

Definition elector {Action : Set} : Type := 
  (list (RegExp * Action)) * (list ((string -> bool) * Action)) -> string -> option (electionResult (Action := Action)).

Fixpoint function_selector {Action : Set} (fun_list: list ((string -> bool) * Action)) str := match fun_list with
| [] => None
| (fn, ac)::t => if fn str then Some (fn, ac) else function_selector t str
end.

Definition generalizing_elector {Action : Set} (regex_elector: list (RegExp * Action) -> string -> option (RegexpElectionResult (Action:=Action))) re_fun str := match re_fun with
| (re_ac, fun_ac) => 
    (
      match function_selector fun_ac str with
      | None => 
        (
          match regex_elector re_ac str with
          | None => None
          | Some can => Some (RegexpElected can)
          end
        )
      | Some (fn, ac) => Some (FunctionElected fn ac)
      end
    )
end.

(* Election - End *)

Definition semantic_action {Token : Set} : Set := Lexbuf -> LexicalAnalysisResult (Token := Token).

Definition exec_sem_action {Token : Set} (sel: electionResult(Action:=semantic_action(Token:=Token))) lexbuf := match sel with
| (FunctionElected _ ac) => ac 
    {|
       s_pos := cur_pos lexbuf;
       lexeme := "";
       cur_pos := cur_pos lexbuf;
       remaining_str := remaining_str lexbuf |}
| (RegexpElected cand) =>
    (electionAction cand)
    {|
       s_pos := cur_pos lexbuf;
       lexeme := cand.(matched);
       cur_pos := {|
                  l_number := l_number (cur_pos lexbuf);
                  c_number := c_number (cur_pos lexbuf) + electionScore cand;
                  abs_pos := abs_pos (cur_pos lexbuf) + electionScore cand |};
       remaining_str := cand.(suffix) |}
end.

