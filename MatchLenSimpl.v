(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex implementation of the optimized version of $S_l:$ #Sl:# *)

Add LoadPath "regexp_opt" as RegExp. 

Require Import RegExp.Definitions.
Require Import RegExp.Utility.
Require Import RegexpSimpl.
Require Import MatchLen.
Require Import Ascii.

Fixpoint match_len re s := 
match re with
| Eps => Some 0
| Empty => None
| _ => ( match s with
        | EmptyString => if nu re then Some 0 else None
        | String a s2 => (match match_len (RegexpSimpl.derive a re) s2 with
                          | Some l => Some (S l)
                          | None => if nu re then Some 0 else None
                          end)
        end )
end.

Lemma match_len_empty : forall s, MatchLen.match_len Empty s = None.
Proof.
induction s; simpl; auto.
rewrite IHs; auto.
Qed.

Lemma match_len_eps : forall s, MatchLen.match_len Eps s = Some 0.
Proof.
induction s; simpl; auto.
rewrite match_len_empty; auto.
Qed.

Lemma match_len_empty' : forall s, match_len Empty s = None.
Proof.
induction s; simpl; auto.
Qed.

Lemma match_len_eps' : forall s, match_len Eps s = Some 0.
Proof.
induction s; simpl; auto.
Qed.

Lemma match_len_re_eq : forall s re re', re =R= re' -> match_len re s = match_len re' s.
Proof.
induction s; simpl.
induction re; auto; intros.
induction re'; auto. 
unfold re_eq in H.
assert (D := H EmptyString).
simpl in D.
inversion D.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
induction re'; auto.
unfold re_eq in H.
assert (D := H EmptyString).
simpl in D.
inversion D.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite <- H; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
rewrite H; induction re'; simpl; auto.
intros.
assert ((derive a re) =R= (derive a re')).
assert (H' := derive_equals re re' H a).
rewrite <- RegexpSimpl.derive_simpl_correct in *.
rewrite <- RegexpSimpl.derive_simpl_correct in *.
auto.
rewrite (IHs (derive a re) (derive a re')); auto.

induction re; auto.
induction re'; auto. 
unfold re_eq in H.
assert (D := H EmptyString).
simpl in D.
inversion D.
simpl (derive a Empty) in H0.
rewrite <- IHs with (re := Empty); auto; simpl.
rewrite match_len_empty'; auto.
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto. 
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto. 
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto. 
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto. 
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := Empty); auto; rewrite match_len_empty'; rewrite <- H; auto.

induction re'; auto. 
unfold re_eq in H.
assert (D := H EmptyString).
simpl in D.
inversion D.
simpl (derive a Eps) in H0.
rewrite <- IHs with (re := Empty); auto; simpl.
assert (D := H EmptyString).
simpl in D.
inversion D.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.
rewrite <- IHs with (re := derive a Eps); auto; rewrite match_len_empty'; rewrite <- H; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.

erewrite nu_equals; eauto.
induction re'; auto.
simpl.
rewrite match_len_empty'; auto.
simpl.
rewrite match_len_empty'; auto.
Qed.

Theorem match_len_correct: forall s re, match_len re s = MatchLen.match_len re s.
Proof.
induction s; simpl; auto.
induction re; auto.
intros.
rewrite match_len_re_eq with (re' := re/a).
rewrite IHs.
induction re; auto.
simpl; rewrite match_len_empty; auto.
simpl; rewrite match_len_empty; auto.
rewrite RegexpSimpl.derive_simpl_correct.
apply re_eq_refl.
Qed.

Fixpoint match_len_term_rec re s term : option nat := 
match re with
| Eps => term (Some 0)
| Empty => term None
| _ => ( match s with
          | EmptyString => term (if nu re then Some 0 else None)
          | String a s2 => 
            (
              match_len_term_rec (RegexpSimpl.derive a re) s2 
                (fun res =>
                  match res with
                  | None => term (if nu re then Some 0 else None)
                  | Some l => term (Some (S l))
                  end
                )
            )
        end)
end.

Theorem general_equivalence_of_match_len_and_match_len_term_rec :
  forall s re term,
    term (match_len re s) = match_len_term_rec re s term.
Proof.
induction s; simpl; auto.
induction re; auto.
intros.
rewrite <- IHs.
case_eq (match_len (derive a re) s);intros; auto.
induction re; auto.
induction re; auto.
Qed.

Definition match_len_terminal_recursive re s := match_len_term_rec re s id.

Theorem general_equivalence_of_match_len_terminal_recursive_correct : 
  forall s re,
    match_len_terminal_recursive re s = match_len re s.
Proof.
unfold match_len_terminal_recursive.
intros.
rewrite <- general_equivalence_of_match_len_and_match_len_term_rec.
unfold id.
auto.
Qed.

Theorem general_equivalence_of_match_len_terminal_recursive_correct2 : 
  forall s re,
    match_len_terminal_recursive re s = MatchLen.match_len re s.
Proof.
intros.
rewrite <- match_len_correct.
apply general_equivalence_of_match_len_terminal_recursive_correct.
Qed.











