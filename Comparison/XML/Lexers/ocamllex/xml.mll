{
    open Buffer
    
    type t_type = 
        | OPEN
        | XMLDeclOpen
        | CLOSE
        | SPECIAL_CLOSE
        | SLASH_CLOSE
        | SLASH
        | EQUALS
        | STRING of string
        | NAME of string
        | CONTENT of string
        | SEA_WS;;
}


(** Verbatim XML definition **)
let ws_carr_re = ['\t' '\n' '\r' ' ']+
let ws_re = ['\t' '\n' ' ']

let ds_punc_re = ['!' '#' '$' '%' '&' '(' ')' '*' '+' ',' '-' '.' '\\' '/' ':' ';' '=' '>' '?' '@' '[' ']' '^' '_' '`' '{' '|' '}' '~' '\'']
let ds_char = ['A' - 'Z'] | ['a' - 'z'] | ['0'-'9'] | ws_re | ds_punc_re
let ds_string = '"' ds_char* '"'

let ss_punc_re = ['!' '#' '$' '%' '&' '(' ')' '*' '+' ',' '-' '.' '\\' '/' ':' ';' '=' '>' '?' '@' '[' ']' '^' '_' '`' '{' '|' '}' '~' ]
let ss_char = ['A' - 'Z'] | ['a' - 'z'] | ['0'-'9'] | ws_re | ss_punc_re
let ss_string = '\'' ss_char* '\''

let c_punc_re = ['!' '#' '$' '%' '&' '(' ')' '*' '+' ',' '-' '.' '\\' ':' ';' '@' '[' ']' '^' '_' '`' '{' '|' '}' '~' '\'']
let c_char_re = ['A' - 'Z'] | ['a' - 'z'] | ['0'-'9'] | ws_re | c_punc_re
let content = c_char_re+


let str = ds_string | ss_string

let name_start = ['A' - 'Z'] | ['a' - 'z'] | ':'
let name_char = name_start | '-' | '_' | '.' | ['0'-'9']
let name = name_start name_char*


(** End of Verbatim XML definition **)

rule read =
  parse
  | '<'           {  OPEN   }
  | "<?xml"       {  XMLDeclOpen   }
  | '>'           {  CLOSE   }
  | "?>"       {  SPECIAL_CLOSE   }
  | "/>"       {  SLASH_CLOSE   }
  | '/'           {  SLASH   }
  | '='           {  EQUALS   }
  | str           { STRING(Lexing.lexeme lexbuf) }
  | content           { CONTENT(Lexing.lexeme lexbuf) }
  | name           { NAME(Lexing.lexeme lexbuf) }
  | ws_carr_re    {  SEA_WS }
(*  | _ { failwith ("Unexpected char: " ^ (Lexing.lexeme lexbuf)) }
  | eof      { EOF }*)
