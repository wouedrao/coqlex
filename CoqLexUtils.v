Add LoadPath "regexp_opt" as RegExp.

Require Import RegExp.Definitions.
Require Import Lia.
Require Import MatchLenSimpl.
Require Import LexerDefinition.
Require Import Coq.Lists.List String.
Import ListNotations.

Definition new_line_f lexbuf := (mkLexbuf 
        (s_pos lexbuf) 
        (lexeme lexbuf) 
        (mkPosition
          (S (l_number (cur_pos lexbuf)))
          0
          (abs_pos (cur_pos lexbuf))
        )
        (remaining_str lexbuf)
      ).

Definition new_line {Token: Set} : semantic_action (Token:=Token) :=
  (fun lexbuf => 
    (AnalysisTerminated None (new_line_f lexbuf))).

Definition ret_nothing {Token: Set} : semantic_action (Token:=Token) := 
  (fun lexbuf => (AnalysisTerminated None lexbuf)).
  
Definition empty {Token: Set} := ret_nothing (Token:=Token).

Definition ret_token {Token: Set} (tok: Token) : semantic_action (Token:=Token) := 
  (fun lexbuf => (AnalysisTerminated (Some tok) lexbuf)).
  
Definition ret {Token: Set} := ret_token (Token:=Token).

Definition ret_lexeme {Token: Set} (c: string -> Token)  : semantic_action (Token:=Token) := 
  (fun lexbuf => (AnalysisTerminated (Some (c lexbuf.(lexeme))) lexbuf)).
  
Definition ret_l {Token: Set} := ret_lexeme (Token:=Token).

Definition ignore {Token Token': Set} (lex : semantic_action (Token:=Token)) : semantic_action (Token:=Token') :=
  (fun lexbuf => (
    match lex lexbuf with
    | (AnalysisTerminated _ lexbuf') => (AnalysisTerminated None lexbuf')
    | (AnalysisFailedUserRaisedError msg lexbuf') => (AnalysisFailedUserRaisedError msg lexbuf')
    | (AnalysisFailedEmptyToken lexbuf') => (AnalysisFailedEmptyToken lexbuf')
    | (AnalysisNoFuel lexbuf') => (AnalysisNoFuel lexbuf')
    | (SubLexemeError msg lexbuf') => (SubLexemeError msg lexbuf')
    end )).

Definition raise {Token: Set} msg : semantic_action (Token:=Token) :=
  (fun lexbuf => (AnalysisFailedUserRaisedError msg lexbuf)).
  
Definition raise_lexeme {Token: Set} (c: string -> string) : semantic_action (Token:=Token) :=
  (fun lexbuf => (AnalysisFailedUserRaisedError (c lexbuf.(lexeme)) lexbuf)).
  
Definition raise_l {Token: Set} := raise_lexeme (Token:=Token).

Fixpoint sequence {Token: Set} (seq: list (semantic_action (Token:=Token))) lexbuf {struct seq} := 
  match seq with
  | [] => (AnalysisTerminated None lexbuf)
  | f::[] => f lexbuf 
  | h::t => 
      (
        match h lexbuf with
        | (AnalysisTerminated tok_opt lexbuf') => sequence t lexbuf'
        | r => r
        end
      )
  end.

Definition EOF str := match str with | EmptyString => true | _ => false end.

Definition default_fuel := 5000.




