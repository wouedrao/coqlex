(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex implementation of $S_s:$ #Ss:# *)

(* 
  ShortestLen (or shortest score) implements the rule of the shortest possible lexeme
  Verification : 
    Important props : 
                  * forall str regex total_s, shortest_match_len regex str = Some total_s -> matches regex (substring 0 total_s str) = true.
                  * forall str regex, shortest_match_len regex str = None <-> (forall m, matches regex (substring 0 m str) = false)
                  * forall str regex total_s result, matches regex (substring 0 total_s str) = true -> shortest_match_len regex str = Some result -> result =< total_s.
    with matches, a verified function returning true if a regex matches a string.
 *)

Add LoadPath "regexp_opt" as RegExp.
Require Import RegExp.Definitions.
Require Import RegExp.Boolean.
Require Import RegExp.Char.
Require Import Coq.Strings.String.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Compare.
Require Import Coq.Arith.PeanoNat.
Require Import Lia.

Fixpoint shortest_match_len re s := 
(
  match s with
  | EmptyString => if nu re then Some 0 else None
  | String a s2 => if nu re then Some 0 else (match shortest_match_len (re/a) s2 with
                  | Some l => Some (S l)
                  | None => None
                  end)
  end
).

Lemma shortest_match_len_correctness_match: forall str regex total_s, shortest_match_len regex str = Some total_s -> matches regex (substring 0 total_s str) = true.
Proof.
induction str; simpl; intros; case_eq (nu regex); intros; rewrite H0 in *; inversion H; subst; simpl; auto; clear H2.
case_eq (shortest_match_len (regex / a) str); intros; rewrite H1 in *; inversion H; subst; simpl; auto; clear H2.
Qed.

Lemma shortest_match_len_correctness_shortest: forall str regex mlen rlen, shortest_match_len regex str = Some rlen -> matches regex (substring 0 mlen str) = true -> rlen <= mlen.
Proof.
induction str; simpl; intros; case_eq (nu regex); intros; rewrite H1 in *; inversion H; subst; try lia; clear H3.
case_eq (shortest_match_len (regex / a) str); intros; rewrite H2 in * ; inversion H; subst; simpl; auto.
induction mlen; simpl in H0.
+ rewrite H0 in H1; inversion H1.
+ assert (H3 := IHstr (regex / a) mlen n H2 H0); lia.
Qed.

Lemma shortest_match_len_completeness : forall str regex, shortest_match_len regex str = None <-> (forall m, matches regex (substring 0 m str) = false).
Proof.
induction str; simpl; intros; case_eq (nu regex); intros.
+ split; intros. 
    inversion H0.
    assert (witness := H0 0). simpl in witness. rewrite H in witness. inversion witness.
+ split; intros; auto.
  induction m; simpl; auto.
+ split; intros.
    inversion H0.
    assert (witness := H0 0). simpl in witness. rewrite H in witness. inversion witness.
+ case_eq (shortest_match_len (regex / a) str); intros.
  - split; intros. inversion H1.
    assert (forall m, (regex / a) ~!= substring 0 m str).
      intros.
      apply (H1 (S m)).
    apply IHstr in H2.
    rewrite H0 in H2; inversion H2.
  - split; auto; intros.
    induction m; simpl; auto.
    apply IHstr.
    auto.
Qed.

Lemma shortest_match_len_re_eq : forall s re re', re =R= re' -> shortest_match_len re s = shortest_match_len re' s.
Proof.
induction s; simpl; intros.
rewrite H; auto.
assert (H' := derive_equals re re' H a).
apply IHs in H'.
rewrite H'.
assert (H'' := nu_equals re re' H).
rewrite H''; auto.
Qed.


