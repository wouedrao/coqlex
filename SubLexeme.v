(* *********************************************************************)
(*                                                                     *)
(*                 Coqlex verified lexer generator                     *)
(*                                                                     *)
(*  Copyright 2022 Siemens Mobility SAS and Institut National de       *)
(*  Recherche en Informatique et en Automatique.                       *)
(*  All rights reserved. This file is distributed under                *)
(*  the terms of the INRIA Non-Commercial License Agreement (see the   *)
(*  LICENSE file).                                                     *)
(*                                                                     *)
(* *********************************************************************)

(** * Coqlex implementation of sublexeme *)

Add LoadPath "regexp_opt" as RegExp.

Require Import RegExp.Definitions RegExp.Boolean RegExp.Concat.
Require Import Lia.
Require Import MatchLenSimpl RegexpSimpl.
Require Import List String.
Import ListNotations.

Fixpoint list_to_cat lst := match lst with
| [] => Eps
| [h] => h
| h::t => Cat h (list_to_cat t)
end.

Inductive correct_cat_cut : list RegExp -> list string -> Prop := 
| correct_cat_cut_nil : correct_cat_cut [] []
| correct_cat_cut_cons : forall rh rt sh st, 
    matches rh sh = true -> correct_cat_cut rt st -> correct_cat_cut (rh::rt) (sh::st).

Fixpoint split_first s l r := match s with
| EmptyString => if ((nu l) && (nu r))%bool then (Some (EmptyString, EmptyString)) else None
| String h t => 
    (
      match split_first t (RegexpSimpl.derive h l) r with
      | Some (ls, rs) => Some (String h ls, rs)
      | None => if ((nu l) && (matches r s))%bool then Some (EmptyString, s) else None
      end
    )
end.

Lemma split_first_some_matches : forall s l r sl sr, split_first s l r = Some (sl, sr) -> (matches l sl = true /\ matches r sr = true).
Proof.
induction s; simpl.
+ intros l r; case_eq (nu l && nu r)%bool; intros; inversion H0; simpl; apply Bool.andb_true_iff; eauto.
+ intros. case_eq (split_first s (derive a l) r); intros; rewrite H0 in H. induction p. inversion H; subst.
  apply IHs in H0.
  destruct H0.
  split; auto.
  rewrite derive_simpl_correct in H0.
  rewrite derivation; auto.
  case_eq ((nu l && (r / a ~= s))%bool); intros; rewrite H1 in H; inversion H.
  simpl.
  apply Bool.andb_true_iff; eauto.
Qed.

Lemma split_first_some_concat : forall s l r sl sr, split_first s l r = Some (sl, sr) -> s = String.append sl sr.
Proof.
induction s; simpl.
+ intros; induction (nu l && nu r)%bool; inversion H; simpl; auto.
+ intros l r; case_eq (split_first s (derive a l) r); intros; 
    [induction p; inversion H0; subst; simpl; f_equal; eauto|];
  induction (nu l && (r / a ~= s))%bool; inversion H0; eauto.
Qed.

Lemma split_first_some_iff : forall s l r, (exists sl sr, split_first s l r = Some (sl, sr)) <-> matches (Cat l r) s = true.
Proof.
induction s; simpl.
+ split; induction (nu l && nu r)%bool; intros; eauto; [|inversion H]; destruct H, H; inversion H.
+ split; intros.
  {
    induction (nu l).
    {
      case_eq (split_first s (derive a l) r); intros; rewrite H0 in H.
      {
        induction p.
        assert (exists a0 b, split_first s (derive a l) r = Some (a0, b)) by eauto.
        apply IHs in H1.
        rewrite (RegExp.Concat.Cat_morphism_s s (derive a l) (l / a) r r) in H1; eauto; [| eapply derive_simpl_correct | eapply re_eq_refl].
        rewrite matches_Or.
        apply Bool.orb_true_intro.
        left; auto.
      }
      {
        rewrite matches_Or.
        apply Bool.orb_true_intro.
        right.
        induction (r / a ~= s); eauto; simpl in H; destruct H, H; inversion H.
      }
    }
    simpl in H.
    case_eq (split_first s (derive a l) r); intros; rewrite H0 in H; [|destruct H, H; inversion H].
    induction p.
    assert (exists a0 b, split_first s (derive a l) r = Some (a0, b)) by eauto.
    apply IHs in H1.
    rewrite <- (RegExp.Concat.Cat_morphism_s s (l / a) (derive a l) r r) in H1; eauto; [apply re_eq_sym; eapply derive_simpl_correct | eapply re_eq_refl].
  }
  {
    induction (nu l); simpl.
    {
      rewrite matches_Or in H.
      apply Bool.Bool.orb_true_iff in H.
      destruct H.
      {
        rewrite  (RegExp.Concat.Cat_morphism_s s (l / a) (derive a l) r r) in H; eauto; [ | apply re_eq_sym;  eapply derive_simpl_correct | eapply re_eq_refl].
        apply IHs in H.
        destruct H, H.
        rewrite H; eauto.
      }
      {
        rewrite H.
        induction (split_first s (derive a l) r); eauto; induction a0; eauto.
      }
    }
    rewrite  (RegExp.Concat.Cat_morphism_s s (l / a) (derive a l) r r) in H; eauto; [ | apply re_eq_sym;  eapply derive_simpl_correct | eapply re_eq_refl].
    apply IHs in H.
    destruct H, H; rewrite H; eauto.
  }
Qed.

Fixpoint split s reLst := match reLst with
| [] => 
  (
    match s with
    | EmptyString => Some []
    | _ => None
    end
  )
| [re] => if matches re s then Some [s] else None
| a::b =>
  (
    match split_first s a (list_to_cat b) with
    | None => None
    | Some (sa, sb) =>
      (
        match split sb b with
        | Some l => Some (sa::l)
        | None => None
        end
      )
    end
  )
end.

(* Compute (split "aaaab"%string [(Star (Char "a"%char)); (Star (Char "a"%char)); (Star(Char "b"%char))]). *)

Lemma split_some_iff: forall re s, (exists c, split s re = Some c) <-> matches (list_to_cat re) s = true.
Proof.
induction re; simpl.
+ intro s; case s; simpl; split; intros; eauto; [destruct H| rewrite Empty_false in H]; inversion H.
+ case_eq re; intros; subst.
  - split; intros; induction (a ~= s); eauto; [destruct H|]; inversion H.
  - case_eq (split_first s a (list_to_cat (r :: l))); intros.
  {
    induction p.
    case_eq (split b (r :: l)); intros.
    {
      split; intros; eauto.
      assert (H' := H).
      apply split_first_some_matches in H.
      destruct H.
      apply split_first_some_concat in H'.
      rewrite H'.
      apply matches_Cat; eauto.
    }
    apply split_first_some_matches in H.
    destruct H.
    apply IHre in H1.
    destruct H1.
    rewrite H1 in H0.
    inversion H0.
  }
  split; intros.
  {
    destruct H0.
    inversion H0.
  }
  apply split_first_some_iff in H0.
  destruct H0, H0; rewrite H0 in H; inversion H.
Qed.

Theorem split_correct_match : forall reL s cut, split s reL = Some cut -> correct_cat_cut reL cut.
Proof.
induction reL; simpl.
+ intro s; case s; intros; inversion H; apply correct_cat_cut_nil.
+ case_eq reL; intros; subst.
  - case_eq (a ~= s); intros; rewrite H in *; inversion H0; econstructor; eauto; econstructor.
  - case_eq (split_first s a (list_to_cat (r :: l))); intros; rewrite H in H0; [|inversion H0]; induction p;
    case_eq (split b (r :: l)); intros; rewrite H1 in H0; inversion H0.
    econstructor; eauto.
    apply split_first_some_matches in H.
    destruct H; auto.
Qed.


Theorem split_correct_concat : forall reL s cut, split s reL = Some cut -> String.concat EmptyString cut = s.
Proof.
induction reL; simpl.
+ intro s; case s; intros; inversion H; auto.
+ case_eq reL; intros; subst.
  - case_eq (a ~= s); intros; rewrite H in *; inversion H0; auto.
  - case_eq (split_first s a (list_to_cat (r :: l))); intros; rewrite H in H0; [|inversion H0]; induction p;
    case_eq (split b (r :: l)); intros; rewrite H1 in H0; inversion H0.
    apply split_first_some_concat in H.
    rewrite H.
    apply IHreL in H1.
    simpl.
    rewrite H1.
    induction l0; auto.
    simpl in H1.
    rewrite <- H1.
    rewrite string_right_id.
    auto.
Qed.


